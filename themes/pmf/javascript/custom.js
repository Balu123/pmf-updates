$(function(){
	$("#navigation2").naver({
	  maxWidth: "767px"
	});
  $('.carousel').carousel({
    interval: 5000
  })
})

$(window).load(function() {
    var bodyheight = $('#body-section').height();
    $("#sidebar").height(bodyheight);
    $("#page-content").height(bodyheight);
    
});

$(document).ready(function() {
   $(window).resize(function() {
      var bodyheight = $('#body-section').height();
      $("#sidebar").height(bodyheight);
      $("#page-content").height(bodyheight);
   }); 
});