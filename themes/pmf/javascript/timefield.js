var ss = ss || {};

(function($) {
	$.entwine('ss', function($) {

		$('input.time').entwine({
			onmatch: function() {
				this.timepicker();
			}
		});

	});
}(jQuery));