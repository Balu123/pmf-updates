<!-- body-section start here -->
<section id="body-section" class="section-common">
	<div class="container">
		

				
				<% if PageHeading %><h3 class="page-heading">$PageHeading</h3><% end_if %>
				$Content

				<% if CurrentMember %><% else %>
				<p>The full text version of most articles is only available on-line to members of the PM Forum. Do you want to <a href="/security/login">log in</a> now?</p>
				<% end_if %>
				<div class="quick-search">
				<div class="knowledge-widget">
					
					<h3>Quick search</h3>
					
					$QuickSearchForm

				</div>

				<div class="knowledge-widget">
					<h3>Advanced search</h3>
					$AdvanceSearchForm
				</div>
				</div>
				<% if $isSearchActive %>
				<h3 class="page-heading">Search Result</h3>
				<h4>$SearchResult.Count Results</h4>

				<% if $SearchResult.Count %>
				<div class="pagi-filter-wrap">
					<% if $SearchResult.MoreThanOnePage %>
					<div class="row">
						<div class="col-sm-5">
							<ul class="pagi-navi">
								<li>Show</li>
								<li><a <% if ShowNumberOfItems == 24 %>class="active"<% end_if %> href="{$SearchViewNoOfItemsLink}24">24</a></li>
								<li><a <% if ShowNumberOfItems == 48 %>class="active"<% end_if %> href="{$SearchViewNoOfItemsLink}48">48</a></li>
								<li><a <% if ShowNumberOfItems == 100 %>class="active"<% end_if %> href="{$SearchViewNoOfItemsLink}100">100</a></li>
								<li>per page</li>
							</ul>
						</div>
						<div class="col-sm-7">
							<ul class="pagi-navi pull-right">
								<% if $SearchResult.NotFirstPage %>
								<li><a class="prev" href="$SearchResult.PrevLink" title="View the previous page">&lt;</a></li>
								<% end_if %>
								
								<% loop $SearchResult.PaginationSummary(5) %>
								<% if $CurrentBool %>
								<li><a class="active" href="$Link" title="View page number $PageNum">$PageNum</a></li>
								<% else %>
								<% if Link %>
								<li><a href="$Link" title="View page number $PageNum">$PageNum</a></li>
								<% else %>
								<li>...</li>
								<% end_if %>
								<% end_if %>
								<% end_loop %>

								<% if $SearchResult.NotLastPage %>
								<li><a class="next" href="$SearchResult.NextLink" title="View the next page">&gt;</a></li>
								<% end_if %>
							</ul>
					
					<% end_if %>            
				
				<% end_if %>
</div>
<div class="row">
			<% loop SearchResult %>
				<div class="col-sm-4">
						<div class="event-item">
							<div class="event-title">
								<div class="event-calendar calendar-bg1">
									<span>$MagazineIssue.FormatedYear</span>
								</div>
								<div class="event-title-desc">
									<h4><a href="$Link">$Title</a></h4>
								</div>
							</div>
							<h5>$ArticleSection.Title</h5>
							<p><% if $Summary %>$Summary<% else %>$Content.Summary<% end_if %></p>
							<a href="$Link" class="btn-further-info">More</a>
						</div>
					</div>
				<% if multipleOf(3) %>
					</div>
					<div class="row">
				<% end_if %>
			<% end_loop %>
		</div>



				<% if $SearchResult.Count %>
				<div class="pagi-filter-wrap">
					<% if $SearchResult.MoreThanOnePage %>
					<div class="row">
						<div class="col-sm-5">
							<ul class="pagi-navi">
								<li>Show</li>
								<li><a <% if ShowNumberOfItems == 24 %>class="active"<% end_if %> href="{$SearchViewNoOfItemsLink}24">24</a></li>
								<li><a <% if ShowNumberOfItems == 48 %>class="active"<% end_if %> href="{$SearchViewNoOfItemsLink}48">48</a></li>
								<li><a <% if ShowNumberOfItems == 100 %>class="active"<% end_if %> href="{$SearchViewNoOfItemsLink}100">100</a></li>
								<li>per page</li>
							</ul>
						</div>
						<div class="col-sm-7">
							<ul class="pagi-navi pull-right">
								<% if $SearchResult.NotFirstPage %>
								<li><a class="prev" href="$SearchResult.PrevLink" title="View the previous page">&lt;</a></li>
								<% end_if %>
								
								<% loop $SearchResult.PaginationSummary(5) %>
								<% if $CurrentBool %>
								<li><a class="active" href="$Link" title="View page number $PageNum">$PageNum</a></li>
								<% else %>
								<% if Link %>
								<li><a href="$Link" title="View page number $PageNum">$PageNum</a></li>
								<% else %>
								<li>...</li>
								<% end_if %>
								<% end_if %>
								<% end_loop %>

								<% if $SearchResult.NotLastPage %>
								<li><a class="next" href="$SearchResult.NextLink" title="View the next page">&gt;</a></li>
								<% end_if %>
							</ul>
						</div>   
					</div> 
					<% end_if %>       
				</div>
				<% end_if %>

				
				<% end_if %>

			</div>
		</div>
	</div>
	
</section>
<!-- body-section end here -->
