<!-- body-section start here -->
<section id="body-section" class="section-common events-page">
	<div class="container">
		<div class="row">
			<% loop MergedEventItems %>
				<% if ClassName == CustomBlock %>
					<% if $Up.ShowCustomBlock %>
					<div class="col-sm-4">
						<div class="event-pannel pannel-conf">
							$Content
						</div>
					</div>
					<% end_if %>
				<% else %>
					<div class="col-sm-4">
						<div class="event-item">
							<h3>$LocationPage.Title</h3>
							<div class="event-title">
								<div class="event-calendar calendar-bg1">
									<strong>$Date.format(j)</strong><br>
									<span>$Date.format(M)</span>
								</div>
								<div class="event-title-desc">
									<h4><a href="$MoreInfoLink">$Title</a></h4>
								</div>
							</div>
							<p>$Description</p>
							<a href="$MoreInfoLink" class="btn-further-info">Further info and RSVP</a>
						</div>
					</div>
				<% end_if %>

				<% if multipleOf(3) %>
				</div>
				<div class="row">
				<% end_if %>
			<% end_loop %>
		</div>
	</div>
</section>
<!-- body-section end here -->
