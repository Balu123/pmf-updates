-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2015 at 01:12 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pmf`
--

-- --------------------------------------------------------

--
-- Table structure for table `BlogEntry`
--

CREATE TABLE IF NOT EXISTS `BlogEntry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `Author` mediumtext,
  `Tags` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `BlogEntry`
--

INSERT INTO `BlogEntry` (`ID`, `Date`, `Author`, `Tags`) VALUES
(14, '2015-01-29 14:05:13', 'Default Admin', 'blog'),
(17, '2015-01-29 14:41:40', 'Default Admin', NULL),
(18, '2015-01-15 14:44:44', 'Default Admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `BlogEntry_Live`
--

CREATE TABLE IF NOT EXISTS `BlogEntry_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `Author` mediumtext,
  `Tags` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `BlogEntry_Live`
--

INSERT INTO `BlogEntry_Live` (`ID`, `Date`, `Author`, `Tags`) VALUES
(14, '2015-01-29 14:05:13', 'Default Admin', 'blog'),
(17, '2015-01-29 14:41:40', 'Default Admin', NULL),
(18, '2015-01-15 14:44:44', 'Default Admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `BlogEntry_versions`
--

CREATE TABLE IF NOT EXISTS `BlogEntry_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` datetime DEFAULT NULL,
  `Author` mediumtext,
  `Tags` mediumtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `BlogHolder`
--

CREATE TABLE IF NOT EXISTS `BlogHolder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AllowCustomAuthors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowFullEntry` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `BlogHolder`
--

INSERT INTO `BlogHolder` (`ID`, `AllowCustomAuthors`, `ShowFullEntry`, `OwnerID`) VALUES
(13, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `BlogHolder_Live`
--

CREATE TABLE IF NOT EXISTS `BlogHolder_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AllowCustomAuthors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowFullEntry` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `BlogHolder_Live`
--

INSERT INTO `BlogHolder_Live` (`ID`, `AllowCustomAuthors`, `ShowFullEntry`, `OwnerID`) VALUES
(13, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `BlogHolder_versions`
--

CREATE TABLE IF NOT EXISTS `BlogHolder_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `AllowCustomAuthors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowFullEntry` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `BlogTree`
--

CREATE TABLE IF NOT EXISTS `BlogTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `LandingPageFreshness` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `BlogTree`
--

INSERT INTO `BlogTree` (`ID`, `Name`, `LandingPageFreshness`) VALUES
(13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `BlogTree_Live`
--

CREATE TABLE IF NOT EXISTS `BlogTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `LandingPageFreshness` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `BlogTree_Live`
--

INSERT INTO `BlogTree_Live` (`ID`, `Name`, `LandingPageFreshness`) VALUES
(13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `BlogTree_versions`
--

CREATE TABLE IF NOT EXISTS `BlogTree_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Name` varchar(255) DEFAULT NULL,
  `LandingPageFreshness` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CommitteeMember`
--

CREATE TABLE IF NOT EXISTS `CommitteeMember` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('CommitteeMember') DEFAULT 'CommitteeMember',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `Designation` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `LocationPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`),
  KEY `ClassName` (`ClassName`),
  KEY `LocationPageID` (`LocationPageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `CommitteeMember`
--

INSERT INTO `CommitteeMember` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `ImageID`, `Designation`, `Email`, `SortOrder`, `LocationPageID`) VALUES
(4, 'CommitteeMember', '2015-02-03 15:01:01', '2015-02-03 18:17:00', 'Sue Carr', 'V Formation', 14, 'Regional Director', 'suecarrmarketing@gmail.com', 1, 19),
(5, 'CommitteeMember', '2015-02-04 16:58:29', '2015-02-04 16:58:29', 'Andy Raynor', 'Shakespeares', 15, 'Chair', NULL, 2, 19),
(6, 'CommitteeMember', '2015-02-04 16:59:49', '2015-02-04 16:59:50', 'Leah Bradley', NULL, 16, 'Regional PR', NULL, 3, 19),
(7, 'CommitteeMember', '2015-02-04 17:00:15', '2015-02-04 17:00:15', 'Trish Burnell', 'MHA MacIntyre Hudson', 17, NULL, NULL, 4, 19),
(8, 'CommitteeMember', '2015-02-04 17:00:40', '2015-02-04 17:00:40', 'Matt Coleman', 'Actons', 18, NULL, NULL, 5, 19),
(9, 'CommitteeMember', '2015-02-04 17:01:05', '2015-02-04 17:01:06', 'Pauline Nicholls', 'Deloitte', 19, NULL, NULL, 6, 19),
(10, 'CommitteeMember', '2015-02-04 17:01:28', '2015-02-04 17:01:28', 'Jennifer Sparks', 'BDO', 20, NULL, NULL, 7, 19),
(11, 'CommitteeMember', '2015-02-04 17:01:58', '2015-02-04 17:01:59', 'Jemma Taylor-Smith', 'Grant Thornton', 21, NULL, NULL, 8, 19);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage`
--

CREATE TABLE IF NOT EXISTS `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ErrorPage`
--

INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage_Live`
--

CREATE TABLE IF NOT EXISTS `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ErrorPage_Live`
--

INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage_versions`
--

CREATE TABLE IF NOT EXISTS `ErrorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Event`
--

CREATE TABLE IF NOT EXISTS `Event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Event') DEFAULT 'Event',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(1000) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Description` mediumtext,
  `LocationPageID` int(11) NOT NULL DEFAULT '0',
  `MoreInfoLink` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LocationPageID` (`LocationPageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Event`
--

INSERT INTO `Event` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Date`, `Description`, `LocationPageID`, `MoreInfoLink`) VALUES
(1, 'Event', '2015-02-03 15:10:57', '2015-02-03 15:10:57', 'Building a business development mindset across the firm', '2015-03-25 00:00:00', '<p>Should ‘Sales’ ever be just a department? In fact, are firms missing a trick if they think that way? Ambitious leaders state that they want to make BD a mindset that runs through the veins of their firms at every level and in every function.</p>\n<p>Pie-in-the sky dreams? Maybe for some, but for others it’s a critical business advantage to have, especially in markets that are revving up. You choose.</p>', 19, 'http://sites.pmi.vuturevx.com/9/568/february-2015/building-a-business-development-mindset-across-the-firm---wednesday-25-march-2015--leicester%281%29.asp?sid=838f7c93-8157-45d9-8846-8bef58e33d0c');

-- --------------------------------------------------------

--
-- Table structure for table `File`
--

CREATE TABLE IF NOT EXISTS `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('File','Folder','Image','Image_Cached') DEFAULT 'File',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Filename` mediumtext,
  `Content` mediumtext,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `File`
--

INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `ParentID`, `OwnerID`) VALUES
(1, 'Folder', '2015-01-23 14:48:27', '2015-01-23 14:48:27', 'Uploads', 'Uploads', 'assets/Uploads/', NULL, 1, 0, 0),
(2, 'File', '2015-01-23 14:48:27', '2015-01-23 14:48:27', 'error-404.html', 'error-404.html', 'assets/error-404.html', NULL, 1, 0, 0),
(3, 'File', '2015-01-23 14:48:27', '2015-01-23 14:48:27', 'error-500.html', 'error-500.html', 'assets/error-500.html', NULL, 1, 0, 0),
(4, 'Image', '2015-01-29 14:07:05', '2015-01-29 14:07:05', 'slide-img1.jpg', 'slide img1', 'assets/Uploads/slide-img1.jpg', NULL, 1, 1, 1),
(5, 'Image', '2015-01-29 14:08:11', '2015-01-29 14:08:11', 'slide-img2.jpg', 'slide img2', 'assets/Uploads/slide-img2.jpg', NULL, 1, 1, 1),
(6, 'Image', '2015-01-29 14:46:04', '2015-01-29 14:46:04', 'latest-news-img.jpg', 'latest news img', 'assets/Uploads/latest-news-img.jpg', NULL, 1, 1, 1),
(7, 'Image', '2015-01-29 17:35:47', '2015-01-29 17:35:47', 'help-icon.png', 'help icon', 'assets/Uploads/help-icon.png', NULL, 1, 1, 1),
(9, 'Folder', '2015-02-03 17:57:00', '2015-02-03 17:57:00', 'training-workshop-forms', 'training-workshop-forms', 'assets/training-workshop-forms/', NULL, 1, 0, 1),
(11, 'Folder', '2015-02-03 18:03:26', '2015-02-03 18:03:26', 'training-workshop-forms', 'training-workshop-forms', 'assets/Uploads/training-workshop-forms/', NULL, 1, 1, 1),
(12, 'File', '2015-02-03 18:03:27', '2015-02-03 18:03:27', 'Project-and-Campaign-Management-2015.pdf', 'Project and Campaign Management 2015', 'assets/Uploads/training-workshop-forms/Project-and-Campaign-Management-2015.pdf', NULL, 1, 11, 1),
(13, 'Folder', '2015-02-03 18:16:57', '2015-02-03 18:16:57', 'committee-members', 'committee-members', 'assets/Uploads/committee-members/', NULL, 1, 1, 1),
(14, 'Image', '2015-02-03 18:16:57', '2015-02-03 18:16:57', 'Sue-Carr.jpg', 'Sue Carr', 'assets/Uploads/committee-members/Sue-Carr.jpg', NULL, 1, 13, 1),
(15, 'Image', '2015-02-04 16:58:26', '2015-02-04 16:58:26', 'Andy-Raynor.jpg', 'Andy Raynor', 'assets/Uploads/committee-members/Andy-Raynor.jpg', NULL, 1, 13, 1),
(16, 'Image', '2015-02-04 16:59:47', '2015-02-04 16:59:47', 'Leah-Bradley.jpg', 'Leah Bradley', 'assets/Uploads/committee-members/Leah-Bradley.jpg', NULL, 1, 13, 1),
(17, 'Image', '2015-02-04 17:00:13', '2015-02-04 17:00:13', 'Trish-Burnell.jpg', 'Trish Burnell', 'assets/Uploads/committee-members/Trish-Burnell.jpg', NULL, 1, 13, 1),
(18, 'Image', '2015-02-04 17:00:38', '2015-02-04 17:00:38', 'Matt-Coleman.jpg', 'Matt Coleman', 'assets/Uploads/committee-members/Matt-Coleman.jpg', NULL, 1, 13, 1),
(19, 'Image', '2015-02-04 17:01:04', '2015-02-04 17:01:04', 'PaulineNicholls.jpg', 'PaulineNicholls', 'assets/Uploads/committee-members/PaulineNicholls.jpg', NULL, 1, 13, 1),
(20, 'Image', '2015-02-04 17:01:26', '2015-02-04 17:01:26', 'Jennifer-Sparks.jpg', 'Jennifer Sparks', 'assets/Uploads/committee-members/Jennifer-Sparks.jpg', NULL, 1, 13, 1),
(21, 'Image', '2015-02-04 17:01:56', '2015-02-04 17:01:56', 'Jemma-Taylor-Smith.jpg', 'Jemma Taylor Smith', 'assets/Uploads/committee-members/Jemma-Taylor-Smith.jpg', NULL, 1, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Group`
--

CREATE TABLE IF NOT EXISTS `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Group') DEFAULT 'Group',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` mediumtext,
  `Code` varchar(255) DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Group`
--

INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `ParentID`) VALUES
(1, 'Group', '2015-01-23 14:48:24', '2015-01-23 14:48:24', 'Content Authors', NULL, 'content-authors', 0, 1, NULL, 0),
(2, 'Group', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'Administrators', NULL, 'administrators', 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Group_Members`
--

CREATE TABLE IF NOT EXISTS `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Group_Members`
--

INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Group_Roles`
--

CREATE TABLE IF NOT EXISTS `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `HomeAdvertisement`
--

CREATE TABLE IF NOT EXISTS `HomeAdvertisement` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('HomeAdvertisement') DEFAULT 'HomeAdvertisement',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(1000) DEFAULT NULL,
  `Link` varchar(1000) DEFAULT NULL,
  `HomePageID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `HomePageID` (`HomePageID`),
  KEY `ImageID` (`ImageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `HomeAdvertisement`
--

INSERT INTO `HomeAdvertisement` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Link`, `HomePageID`, `ImageID`, `SortOrder`) VALUES
(2, 'HomeAdvertisement', '2015-01-29 17:36:17', '2015-01-29 17:36:18', 'Need some help? Ask PMF Advice…', NULL, 1, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `HomeBanner`
--

CREATE TABLE IF NOT EXISTS `HomeBanner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('HomeBanner') DEFAULT 'HomeBanner',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `ExternalLink` varchar(500) DEFAULT NULL,
  `LinkType` enum('External Link','Internal Link') DEFAULT 'External Link',
  `OpenInNewWindow` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `HomePageID` int(11) NOT NULL DEFAULT '0',
  `InternalLinkID` int(11) NOT NULL DEFAULT '0',
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `HomePageID` (`HomePageID`),
  KEY `InternalLinkID` (`InternalLinkID`),
  KEY `ClassName` (`ClassName`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `HomeBanner`
--

INSERT INTO `HomeBanner` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `ExternalLink`, `LinkType`, `OpenInNewWindow`, `SortOrder`, `HomePageID`, `InternalLinkID`, `BannerImageID`) VALUES
(1, 'HomeBanner', '2015-01-29 14:07:17', '2015-01-29 14:10:15', 'first', NULL, 'Internal Link', 0, 2, 1, 12, 4),
(2, 'HomeBanner', '2015-01-29 14:08:18', '2015-01-29 14:10:15', 'second', NULL, 'Internal Link', 0, 1, 1, 12, 5);

-- --------------------------------------------------------

--
-- Table structure for table `HomePage`
--

CREATE TABLE IF NOT EXISTS `HomePage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ContentHeading` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `HomePage`
--

INSERT INTO `HomePage` (`ID`, `ContentHeading`) VALUES
(1, 'About PM Forum');

-- --------------------------------------------------------

--
-- Table structure for table `HomePage_Live`
--

CREATE TABLE IF NOT EXISTS `HomePage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ContentHeading` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `HomePage_Live`
--

INSERT INTO `HomePage_Live` (`ID`, `ContentHeading`) VALUES
(1, 'About PM Forum');

-- --------------------------------------------------------

--
-- Table structure for table `HomePage_versions`
--

CREATE TABLE IF NOT EXISTS `HomePage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ContentHeading` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `LoginAttempt`
--

CREATE TABLE IF NOT EXISTS `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('LoginAttempt') DEFAULT 'LoginAttempt',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Status` enum('Success','Failure') DEFAULT 'Success',
  `IP` varchar(255) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Member`
--

CREATE TABLE IF NOT EXISTS `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Member') DEFAULT 'Member',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Email` varchar(254) DEFAULT NULL,
  `TempIDHash` varchar(160) DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `RememberLoginToken` varchar(160) DEFAULT NULL,
  `NumVisit` int(11) NOT NULL DEFAULT '0',
  `LastVisited` datetime DEFAULT NULL,
  `AutoLoginHash` varchar(160) DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `DateFormat` varchar(30) DEFAULT NULL,
  `TimeFormat` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Email` (`Email`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Member`
--

INSERT INTO `Member` (`ID`, `ClassName`, `Created`, `LastEdited`, `FirstName`, `Surname`, `Email`, `TempIDHash`, `TempIDExpired`, `Password`, `RememberLoginToken`, `NumVisit`, `LastVisited`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `DateFormat`, `TimeFormat`) VALUES
(1, 'Member', '2015-01-23 14:48:27', '2015-02-04 13:30:38', 'Default Admin', NULL, 'admin', '7a7ae6cf425074f328bdb6036a0a8469c5b7f16f', '2015-02-07 13:30:38', '$2y$10$6b15d02305482477a0ff3uMV2RBDqUviUmzSxZRgNCLCtItVpiNRK', NULL, 7, '2015-02-04 21:39:34', NULL, NULL, 'blowfish', '10$6b15d02305482477a0ff36', NULL, NULL, 'en_US', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MemberPassword`
--

CREATE TABLE IF NOT EXISTS `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('MemberPassword') DEFAULT 'MemberPassword',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `MemberPassword`
--

INSERT INTO `MemberPassword` (`ID`, `ClassName`, `Created`, `LastEdited`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES
(1, 'MemberPassword', '2015-01-23 14:48:27', '2015-01-23 14:48:27', '$2y$10$6b15d02305482477a0ff3uMV2RBDqUviUmzSxZRgNCLCtItVpiNRK', '10$6b15d02305482477a0ff36', 'blowfish', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BannerContent` mediumtext,
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Page`
--

INSERT INTO `Page` (`ID`, `BannerContent`, `BannerImageID`) VALUES
(12, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Page_Live`
--

CREATE TABLE IF NOT EXISTS `Page_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BannerContent` mediumtext,
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Page_Live`
--

INSERT INTO `Page_Live` (`ID`, `BannerContent`, `BannerImageID`) VALUES
(12, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Page_versions`
--

CREATE TABLE IF NOT EXISTS `Page_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `BannerContent` mediumtext,
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Page_versions`
--

INSERT INTO `Page_versions` (`ID`, `RecordID`, `Version`, `BannerContent`, `BannerImageID`) VALUES
(1, 12, 4, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Permission`
--

CREATE TABLE IF NOT EXISTS `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Permission') DEFAULT 'Permission',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `Permission`
--

INSERT INTO `Permission` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `Arg`, `Type`, `GroupID`) VALUES
(1, 'Permission', '2015-01-23 14:48:24', '2015-01-23 14:48:24', 'CMS_ACCESS_CMSMain', 0, 1, 1),
(2, 'Permission', '2015-01-23 14:48:24', '2015-01-23 14:48:24', 'CMS_ACCESS_AssetAdmin', 0, 1, 1),
(3, 'Permission', '2015-01-23 14:48:24', '2015-01-23 14:48:24', 'CMS_ACCESS_ReportAdmin', 0, 1, 1),
(4, 'Permission', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'SITETREE_REORGANISE', 0, 1, 1),
(5, 'Permission', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'ADMIN', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionRole`
--

CREATE TABLE IF NOT EXISTS `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRole') DEFAULT 'PermissionRole',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionRoleCode`
--

CREATE TABLE IF NOT EXISTS `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRoleCode') DEFAULT 'PermissionRoleCode',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Quote`
--

CREATE TABLE IF NOT EXISTS `Quote` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Quote') DEFAULT 'Quote',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Content` mediumtext,
  `Author` varchar(50) DEFAULT NULL,
  `Designation` varchar(50) DEFAULT NULL,
  `Company` varchar(50) DEFAULT NULL,
  `HomePageID` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `HomePageID` (`HomePageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Quote`
--

INSERT INTO `Quote` (`ID`, `ClassName`, `Created`, `LastEdited`, `Content`, `Author`, `Designation`, `Company`, `HomePageID`, `SortOrder`) VALUES
(1, 'Quote', '2015-01-29 17:31:19', '2015-01-29 17:31:20', '"PM Forum has vastly improved the confidence of professional marketers that they are adding value."', 'Jonathan Preston', 'Marketing &amp; BD Director', 'SGH Martineau', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage`
--

CREATE TABLE IF NOT EXISTS `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `RedirectorPage`
--

INSERT INTO `RedirectorPage` (`ID`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES
(7, 'Internal', NULL, 6);

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage_Live`
--

CREATE TABLE IF NOT EXISTS `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `RedirectorPage_Live`
--

INSERT INTO `RedirectorPage_Live` (`ID`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES
(7, 'Internal', NULL, 6);

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage_versions`
--

CREATE TABLE IF NOT EXISTS `RedirectorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig`
--

CREATE TABLE IF NOT EXISTS `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteConfig') DEFAULT 'SiteConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Tagline` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `FaceBookLink` varchar(1000) DEFAULT NULL,
  `TwitterLink` varchar(1000) DEFAULT NULL,
  `LinkedInLink` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `SiteConfig`
--

INSERT INTO `SiteConfig` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Tagline`, `Theme`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `FaceBookLink`, `TwitterLink`, `LinkedInLink`) VALUES
(1, 'SiteConfig', '2015-01-23 14:48:25', '2015-01-29 13:51:20', 'PMF', 'Insight inspiration for professional marketers. ', NULL, 'Anyone', 'LoggedInUsers', 'LoggedInUsers', NULL, 'https://twitter.com/PMFGlobal', 'https://www.linkedin.com/groups?gid=1026977');

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_CreateTopLevelGroups`
--

CREATE TABLE IF NOT EXISTS `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_EditorGroups`
--

CREATE TABLE IF NOT EXISTS `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_ViewerGroups`
--

CREATE TABLE IF NOT EXISTS `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree`
--

CREATE TABLE IF NOT EXISTS `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteTree','Page','GridFieldPage','BlogEntry','GridFieldPageHolder','BlogTree','BlogHolder','ErrorPage','RedirectorPage','VirtualPage','BookReviewsPage','ContactPage','EventsPage','HomePage','LocationHolderPage','LocationPage','TrainingPage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `SiteTree`
--

INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Version`, `ParentID`) VALUES
(1, 'HomePage', '2015-01-23 14:48:25', '2015-01-29 17:47:15', 'home', 'Home', NULL, '<p>Nullam nec felis in tellus posuere egestas eget volutpat mauris. In hac habitasse platea dictumst. Nullam at ipsum tellus. Vivamus lobortis gravida nisi ac suscipit. In nibh velit, elementum a elementum convallis, dignissim quis mauris. Nam malesuada, mi et viverra cursus, lorem arcu tincidunt mauris, non aliquam sem est ut elit. Praesent a mi vel ligula aliquet sagittis.</p>\n<ul class="about-post-list"><li><a href="#">Membership rates</a></li>\n<li><a href="#">How can you Benefit?</a></li>\n<li><a href="#">FAQ’s</a></li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 3, 0),
(2, 'Page', '2015-01-23 14:48:25', '2015-01-23 14:48:27', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(3, 'ContactPage', '2015-01-23 14:48:25', '2015-01-27 16:04:43', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 0, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(4, 'ErrorPage', '2015-01-23 14:48:26', '2015-01-23 14:48:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(5, 'ErrorPage', '2015-01-23 14:48:26', '2015-01-23 14:48:28', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(6, 'BookReviewsPage', '2015-01-27 14:39:40', '2015-01-27 14:43:58', 'book-review', 'Book Review', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 4, 7),
(7, 'RedirectorPage', '2015-01-27 14:43:11', '2015-01-27 14:44:30', 'knowledge', 'Knowledge', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(8, 'LocationHolderPage', '2015-01-27 14:45:09', '2015-02-02 20:54:38', 'locations', 'Locations', NULL, '<p>If you would like to receive regular invites to events, please <a href="#">register</a> online.</p>\n<p><strong>Benefits of corporate membership are:</strong></p>\n<ul class="list-benifits"><li>Multiple free subscriptions to <a href="#">PM magazine</a> (10 issues per year)</li>\n<li>Free educational <a href="#">core events</a> with networking opportunities nationally and internationally</li>\n<li>Invitations to member only <a href="#">training workshops</a> and the <a href="#">annual conference</a></li>\n<li>Full access to the <a href="#">article bank</a> with 4,000 keyworded articles</li>\n<li>Free regular <a href="#">research surveys</a></li>\n<li>LawNet members are entitled to 25% discount on PMF membership fees</li>\n<li>Events recognised by CIM for CPD purposes.</li>\n<li>Preferential rate for <a href="#">research surveys</a> at <a href="#">jobs.pmforumglobal.com</a></li>\n<li><a href="#">PMF Advice</a> to avoid hours of research or dead ends.</li>\n<li>Free PM Forum membership for <a href="#">redundant marketers</a></li>\n</ul>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 7, 0),
(9, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:47:48', 'events', 'Events', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 6, 0),
(10, 'Page', '2015-01-27 14:48:29', '2015-01-27 14:48:57', 'pm-magazine', 'PM Magazine', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 3, 0),
(11, 'Page', '2015-01-27 14:50:39', '2015-01-27 14:51:09', 'careers', 'Careers', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 3, 0),
(12, 'TrainingPage', '2015-01-27 14:51:17', '2015-02-03 17:42:24', 'training', 'Training', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 4, 0),
(13, 'BlogHolder', '2015-01-29 14:05:13', '2015-02-03 15:39:13', 'blog', 'Blog', NULL, NULL, NULL, NULL, 0, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(14, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:56:10', 'salaries-modest-growth', 'Salaries – modest growth', NULL, '<p>PM Forum members were asked to complete our annual online survey of marketing salaries. Overall the results confirm that salaries show modest growth after a year of flat lining.</p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 5, 13),
(17, 'BlogEntry', '2015-01-29 14:41:40', '2015-01-29 14:56:35', 'and-the-winner-is', 'And the winner is…', NULL, '<p>The winners for the 2014 MPF awards were celebrated at a gala dinner on 12 March in London.</p>', NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 3, 13),
(18, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:57:58', 'new-rds-appointed', 'New RDs appointed', NULL, '<p><img class="leftAlone grid-list-img; grid-list-img" title="" src="assets/Uploads/latest-news-img.jpg" alt="latest news img" width="120" height="104">Sue Carr, Director of Midlands marketing consultancy V Formation, has been appointed Regional Director for the PM Forum in the East Midlands while Jo Miners of Stonehouse Marketing has taken over in West Midlands.</p>', NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 5, 13),
(19, 'LocationPage', '2015-02-02 20:42:21', '2015-02-03 14:44:28', 'east-midlands', 'East Midlands', NULL, '<p>The <strong class="committee">PM Forum</strong> is a 4,000 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The East Midlands regional <a href="#sec_Events">events</a> take place in <strong>Nottingham</strong> city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. <a href="about/membership.aspx">Sign up here</a>! If you are interested in hosting an event please contact your Regional Director <a href="mailto:sue@vformation.biz"><strong class="committee">Sue Carr</strong></a>.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6, 8),
(20, 'LocationPage', '2015-02-02 20:48:10', '2015-02-02 20:48:17', 'ireland', 'Ireland', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(21, 'LocationPage', '2015-02-02 20:48:29', '2015-02-02 20:48:34', 'london', 'London', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(22, 'LocationPage', '2015-02-02 20:48:41', '2015-02-02 20:48:52', 'north-west', 'North West', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(23, 'LocationPage', '2015-02-02 20:48:56', '2015-02-02 20:58:34', 'scotland', 'Scotland', NULL, '<p>The PM Forum is a 4,500 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The Scotland regional events take place in Edinburgh and Glasgow city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. Sign up here! If you are interested in hosting an event please contact Sophie Hayes.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 3, 8),
(24, 'LocationPage', '2015-02-02 20:49:44', '2015-02-02 20:49:50', 'south-west', 'South West', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(25, 'LocationPage', '2015-02-02 20:50:30', '2015-02-02 20:50:36', 'west-midlands', 'West Midlands', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(26, 'LocationPage', '2015-02-02 20:50:55', '2015-02-02 20:51:00', 'yorkshire', 'Yorkshire', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_EditorGroups`
--

CREATE TABLE IF NOT EXISTS `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_ImageTracking`
--

CREATE TABLE IF NOT EXISTS `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `FileID` (`FileID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `SiteTree_ImageTracking`
--

INSERT INTO `SiteTree_ImageTracking` (`ID`, `SiteTreeID`, `FileID`, `FieldName`) VALUES
(15, 18, 6, 'Content');

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_LinkTracking`
--

CREATE TABLE IF NOT EXISTS `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_Live`
--

CREATE TABLE IF NOT EXISTS `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteTree','Page','GridFieldPage','BlogEntry','GridFieldPageHolder','BlogTree','BlogHolder','ErrorPage','RedirectorPage','VirtualPage','BookReviewsPage','ContactPage','EventsPage','HomePage','LocationHolderPage','LocationPage','TrainingPage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `SiteTree_Live`
--

INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Version`, `ParentID`) VALUES
(1, 'HomePage', '2015-01-23 14:48:25', '2015-01-29 17:47:15', 'home', 'Home', NULL, '<p>Nullam nec felis in tellus posuere egestas eget volutpat mauris. In hac habitasse platea dictumst. Nullam at ipsum tellus. Vivamus lobortis gravida nisi ac suscipit. In nibh velit, elementum a elementum convallis, dignissim quis mauris. Nam malesuada, mi et viverra cursus, lorem arcu tincidunt mauris, non aliquam sem est ut elit. Praesent a mi vel ligula aliquet sagittis.</p>\n<ul class="about-post-list"><li><a href="#">Membership rates</a></li>\n<li><a href="#">How can you Benefit?</a></li>\n<li><a href="#">FAQ’s</a></li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 3, 0),
(2, 'Page', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(3, 'ContactPage', '2015-01-23 14:48:25', '2015-01-27 16:04:43', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 0, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(4, 'ErrorPage', '2015-01-23 14:48:26', '2015-01-23 14:48:32', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(5, 'ErrorPage', '2015-01-23 14:48:26', '2015-01-23 14:48:27', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(6, 'BookReviewsPage', '2015-01-27 14:39:40', '2015-01-27 14:43:58', 'book-review', 'Book Review', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 4, 7),
(7, 'RedirectorPage', '2015-01-27 14:43:11', '2015-01-27 14:44:30', 'knowledge', 'Knowledge', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(8, 'LocationHolderPage', '2015-01-27 14:45:09', '2015-02-02 20:54:39', 'locations', 'Locations', NULL, '<p>If you would like to receive regular invites to events, please <a href="#">register</a> online.</p>\n<p><strong>Benefits of corporate membership are:</strong></p>\n<ul class="list-benifits"><li>Multiple free subscriptions to <a href="#">PM magazine</a> (10 issues per year)</li>\n<li>Free educational <a href="#">core events</a> with networking opportunities nationally and internationally</li>\n<li>Invitations to member only <a href="#">training workshops</a> and the <a href="#">annual conference</a></li>\n<li>Full access to the <a href="#">article bank</a> with 4,000 keyworded articles</li>\n<li>Free regular <a href="#">research surveys</a></li>\n<li>LawNet members are entitled to 25% discount on PMF membership fees</li>\n<li>Events recognised by CIM for CPD purposes.</li>\n<li>Preferential rate for <a href="#">research surveys</a> at <a href="#">jobs.pmforumglobal.com</a></li>\n<li><a href="#">PMF Advice</a> to avoid hours of research or dead ends.</li>\n<li>Free PM Forum membership for <a href="#">redundant marketers</a></li>\n</ul>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 7, 0),
(9, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:47:48', 'events', 'Events', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 6, 0),
(10, 'Page', '2015-01-27 14:48:29', '2015-01-27 14:48:57', 'pm-magazine', 'PM Magazine', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 3, 0),
(11, 'Page', '2015-01-27 14:50:39', '2015-01-27 14:51:10', 'careers', 'Careers', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 3, 0),
(12, 'TrainingPage', '2015-01-27 14:51:17', '2015-02-03 17:42:24', 'training', 'Training', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 4, 0),
(13, 'BlogHolder', '2015-01-29 14:05:13', '2015-02-03 15:39:13', 'blog', 'Blog', NULL, NULL, NULL, NULL, 0, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(14, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:56:10', 'salaries-modest-growth', 'Salaries – modest growth', NULL, '<p>PM Forum members were asked to complete our annual online survey of marketing salaries. Overall the results confirm that salaries show modest growth after a year of flat lining.</p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 5, 13),
(17, 'BlogEntry', '2015-01-29 14:41:40', '2015-01-29 14:56:36', 'and-the-winner-is', 'And the winner is…', NULL, '<p>The winners for the 2014 MPF awards were celebrated at a gala dinner on 12 March in London.</p>', NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 3, 13),
(18, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:57:58', 'new-rds-appointed', 'New RDs appointed', NULL, '<p><img class="leftAlone grid-list-img; grid-list-img" title="" src="assets/Uploads/latest-news-img.jpg" alt="latest news img" width="120" height="104">Sue Carr, Director of Midlands marketing consultancy V Formation, has been appointed Regional Director for the PM Forum in the East Midlands while Jo Miners of Stonehouse Marketing has taken over in West Midlands.</p>', NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 5, 13),
(19, 'LocationPage', '2015-02-02 20:42:21', '2015-02-03 14:44:28', 'east-midlands', 'East Midlands', NULL, '<p>The <strong class="committee">PM Forum</strong> is a 4,000 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The East Midlands regional <a href="#sec_Events">events</a> take place in <strong>Nottingham</strong> city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. <a href="about/membership.aspx">Sign up here</a>! If you are interested in hosting an event please contact your Regional Director <a href="mailto:sue@vformation.biz"><strong class="committee">Sue Carr</strong></a>.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6, 8),
(20, 'LocationPage', '2015-02-02 20:48:10', '2015-02-02 20:48:17', 'ireland', 'Ireland', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(21, 'LocationPage', '2015-02-02 20:48:29', '2015-02-02 20:48:35', 'london', 'London', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(22, 'LocationPage', '2015-02-02 20:48:41', '2015-02-02 20:48:52', 'north-west', 'North West', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(23, 'LocationPage', '2015-02-02 20:48:56', '2015-02-02 20:58:34', 'scotland', 'Scotland', NULL, '<p>The PM Forum is a 4,500 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The Scotland regional events take place in Edinburgh and Glasgow city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. Sign up here! If you are interested in hosting an event please contact Sophie Hayes.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 3, 8),
(24, 'LocationPage', '2015-02-02 20:49:44', '2015-02-02 20:49:50', 'south-west', 'South West', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(25, 'LocationPage', '2015-02-02 20:50:30', '2015-02-02 20:50:36', 'west-midlands', 'West Midlands', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8),
(26, 'LocationPage', '2015-02-02 20:50:55', '2015-02-02 20:51:01', 'yorkshire', 'Yorkshire', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_versions`
--

CREATE TABLE IF NOT EXISTS `SiteTree_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SiteTree','Page','GridFieldPage','BlogEntry','GridFieldPageHolder','BlogTree','BlogHolder','ErrorPage','RedirectorPage','VirtualPage','BookReviewsPage','ContactPage','EventsPage','HomePage','LocationHolderPage','LocationPage','TrainingPage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `SiteTree_versions`
--

INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `ParentID`) VALUES
(1, 1, 1, 1, 0, 0, 'Page', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>. You can now access the <a href="http://doc.silverstripe.org">developer documentation</a>, or begin <a href="http://doc.silverstripe.org/doku.php?id=tutorials">the tutorials.</a></p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(2, 2, 1, 1, 0, 0, 'Page', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(3, 3, 1, 1, 0, 0, 'Page', '2015-01-23 14:48:25', '2015-01-23 14:48:25', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(4, 4, 1, 1, 0, 0, 'ErrorPage', '2015-01-23 14:48:26', '2015-01-23 14:48:26', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(5, 5, 1, 1, 0, 0, 'ErrorPage', '2015-01-23 14:48:26', '2015-01-23 14:48:26', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(6, 1, 2, 1, 1, 1, 'HomePage', '2015-01-23 14:48:25', '2015-01-27 14:39:19', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>. You can now access the <a href="http://doc.silverstripe.org">developer documentation</a>, or begin <a href="http://doc.silverstripe.org/doku.php?id=tutorials">the tutorials.</a></p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(7, 6, 1, 0, 1, 0, 'BookReviewsPage', '2015-01-27 14:39:40', '2015-01-27 14:39:40', 'new-book-reviews-page', 'New Book Reviews Page', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(8, 7, 1, 0, 1, 0, 'Page', '2015-01-27 14:43:11', '2015-01-27 14:43:11', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(9, 7, 2, 1, 1, 1, 'Page', '2015-01-27 14:43:11', '2015-01-27 14:43:18', 'knowledge', 'Knowledge', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(10, 7, 3, 1, 1, 1, 'Page', '2015-01-27 14:43:11', '2015-01-27 14:43:27', 'knowledge', 'Knowledge', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(11, 6, 2, 1, 1, 1, 'BookReviewsPage', '2015-01-27 14:39:40', '2015-01-27 14:43:49', 'book-review', 'Book Review', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(12, 6, 3, 0, 1, 0, 'BookReviewsPage', '2015-01-27 14:39:40', '2015-01-27 14:43:53', 'book-review', 'Book Review', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 7),
(13, 6, 4, 1, 1, 1, 'BookReviewsPage', '2015-01-27 14:39:40', '2015-01-27 14:43:53', 'book-review', 'Book Review', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 7),
(14, 7, 4, 1, 1, 1, 'RedirectorPage', '2015-01-27 14:43:11', '2015-01-27 14:44:17', 'knowledge', 'Knowledge', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(15, 7, 5, 1, 1, 1, 'RedirectorPage', '2015-01-27 14:43:11', '2015-01-27 14:44:30', 'knowledge', 'Knowledge', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(16, 8, 1, 0, 1, 0, 'LocationPage', '2015-01-27 14:45:09', '2015-01-27 14:45:09', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(17, 8, 2, 1, 1, 1, 'LocationPage', '2015-01-27 14:45:09', '2015-01-27 14:45:19', 'location', 'Location', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(18, 8, 3, 1, 1, 1, 'LocationPage', '2015-01-27 14:45:09', '2015-01-27 14:45:26', 'location', 'Location', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(19, 8, 4, 1, 1, 1, 'LocationPage', '2015-01-27 14:45:09', '2015-01-27 14:45:45', 'locations', 'Locations', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(20, 8, 5, 1, 1, 1, 'LocationPage', '2015-01-27 14:45:09', '2015-01-27 14:46:39', 'locations', 'Locations', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(21, 9, 1, 0, 1, 0, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:46:54', 'new-events-page', 'New Events Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(22, 9, 2, 0, 1, 0, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:46:59', 'new-events-page', 'New Events Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 2),
(23, 9, 3, 0, 1, 0, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:47:03', 'new-events-page', 'New Events Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(24, 9, 4, 0, 1, 0, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:47:03', 'new-events-page', 'New Events Page', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(25, 9, 5, 1, 1, 1, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:47:17', 'events', 'Events', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(26, 9, 6, 1, 1, 1, 'EventsPage', '2015-01-27 14:46:54', '2015-01-27 14:47:36', 'events', 'Events', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(27, 10, 1, 0, 1, 0, 'Page', '2015-01-27 14:48:29', '2015-01-27 14:48:29', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(28, 10, 2, 1, 1, 1, 'Page', '2015-01-27 14:48:29', '2015-01-27 14:48:50', 'pm-magazine', 'PM Magazine', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(29, 10, 3, 1, 1, 1, 'Page', '2015-01-27 14:48:29', '2015-01-27 14:48:54', 'pm-magazine', 'PM Magazine', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(30, 11, 1, 0, 1, 0, 'Page', '2015-01-27 14:50:39', '2015-01-27 14:50:39', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(31, 11, 2, 1, 1, 1, 'Page', '2015-01-27 14:50:39', '2015-01-27 14:50:57', 'careers', 'Careers', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(32, 11, 3, 1, 1, 1, 'Page', '2015-01-27 14:50:39', '2015-01-27 14:51:06', 'careers', 'Careers', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(33, 12, 1, 0, 1, 0, 'Page', '2015-01-27 14:51:17', '2015-01-27 14:51:17', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 11, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(34, 12, 2, 1, 1, 1, 'Page', '2015-01-27 14:51:17', '2015-01-27 14:51:27', 'training', 'Training', NULL, NULL, NULL, NULL, 1, 1, 11, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(35, 12, 3, 1, 1, 1, 'Page', '2015-01-27 14:51:17', '2015-01-27 14:51:33', 'training', 'Training', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(36, 3, 2, 1, 1, 1, 'ContactPage', '2015-01-23 14:48:25', '2015-01-27 16:04:43', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>', NULL, NULL, 0, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(37, 13, 1, 1, 1, 1, 'BlogHolder', '2015-01-29 14:05:13', '2015-01-29 14:05:13', 'blog', 'Blog', NULL, NULL, NULL, NULL, 1, 1, 12, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(38, 14, 1, 1, 1, 1, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:05:13', 'sample-blog-entry', 'SilverStripe blog module successfully installed', NULL, '<p>Congratulations, the SilverStripe blog module has been successfully installed. This blog entry can be safely deleted. You can configure aspects of your blog in <a href="admin">the CMS</a>.</p>', NULL, NULL, 0, 1, 0, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(39, 15, 1, 0, 1, 0, 'GridFieldPageHolder', '2015-01-29 14:36:37', '2015-01-29 14:36:37', 'new-grid-field-page-holder', 'New Grid Field Page Holder', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 1),
(40, 15, 2, 0, 1, 0, 'GridFieldPageHolder', '2015-01-29 14:36:37', '2015-01-29 14:36:43', 'new-grid-field-page-holder', 'New Grid Field Page Holder', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(41, 15, 3, 0, 1, 0, 'GridFieldPageHolder', '2015-01-29 14:36:37', '2015-01-29 14:36:46', 'new-grid-field-page-holder', 'New Grid Field Page Holder', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(42, 15, 4, 1, 1, 1, 'GridFieldPageHolder', '2015-01-29 14:36:37', '2015-01-29 14:36:46', 'new-grid-field-page-holder', 'New Grid Field Page Holder', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(43, 16, 1, 1, 1, 1, 'GridFieldPage', '2015-01-29 14:36:54', '2015-01-29 14:36:54', 'new-grid-field-page', 'New Grid Field Page', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 15),
(44, 16, 2, 1, 1, 1, 'GridFieldPage', '2015-01-29 14:36:54', '2015-01-29 14:37:48', 'new-grid-field-page', 'New Grid Field Page', NULL, '<p>csdcs</p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 15),
(45, 14, 2, 0, 1, 0, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:41:22', 'sample-blog-entry', 'SilverStripe blog module successfully installed', NULL, '<p>Congratulations, the SilverStripe blog module has been successfully installed. This blog entry can be safely deleted. You can configure aspects of your blog in <a href="admin">the CMS</a>.</p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(46, 17, 1, 1, 1, 1, 'BlogEntry', '2015-01-29 14:41:40', '2015-01-29 14:41:40', 'new-blog-entry-page', 'New Blog Entry Page', NULL, NULL, NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(47, 14, 3, 1, 1, 1, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:43:33', 'salaries-modest-growth', 'Salaries – modest growth', NULL, '<p>Congratulations, the SilverStripe blog module has been successfully installed. This blog entry can be safely deleted. You can configure aspects of your blog in <a href="admin">the CMS</a>.</p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(48, 14, 4, 1, 1, 1, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:43:55', 'salaries-modest-growth', 'Salaries – modest growth', NULL, '<p><span>PM Forum members were asked to complete our annual online survey of marketing salaries. Overall the results confirm that salaries show modest growth after a year of flat lining.</span></p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(49, 17, 2, 1, 1, 1, 'BlogEntry', '2015-01-29 14:41:40', '2015-01-29 14:44:27', 'and-the-winner-is', 'And the winner is…', NULL, '<p><span>The winners for the 2014 MPF awards were celebrated at a gala dinner on 12 March in London.</span></p>', NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(50, 18, 1, 0, 1, 0, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:44:44', 'new-blog-entry-page', 'New Blog Entry Page', NULL, NULL, NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(51, 18, 2, 1, 1, 1, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:48:30', 'new-rds-appointed', 'New RDs appointed', NULL, '<p><span><img class="leftAlone" title="" src="assets/Uploads/latest-news-img.jpg" alt="latest news img" width="120" height="104">Sue Carr, Director of Midlands marketing consultancy V Formation, has been appointed Regional Director for the PM Forum in the East Midlands while Jo Miners of Stonehouse Marketing has taken over in West Midlands.</span></p>', NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(52, 18, 3, 1, 1, 1, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:48:54', 'new-rds-appointed', 'New RDs appointed', NULL, '<p><span><img class="leftAlone grid-list-img;" title="" src="assets/Uploads/latest-news-img.jpg" alt="latest news img" width="120" height="104">Sue Carr, Director of Midlands marketing consultancy V Formation, has been appointed Regional Director for the PM Forum in the East Midlands while Jo Miners of Stonehouse Marketing has taken over in West Midlands.</span></p>', NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(53, 18, 4, 1, 1, 1, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:55:41', 'new-rds-appointed', 'New RDs appointed', NULL, '<p><img class="leftAlone grid-list-img;" title="" src="assets/Uploads/latest-news-img.jpg" alt="latest news img" width="120" height="104">Sue Carr, Director of Midlands marketing consultancy V Formation, has been appointed Regional Director for the PM Forum in the East Midlands while Jo Miners of Stonehouse Marketing has taken over in West Midlands.</p>', NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(54, 14, 5, 1, 1, 1, 'BlogEntry', '2015-01-29 14:05:13', '2015-01-29 14:56:10', 'salaries-modest-growth', 'Salaries – modest growth', NULL, '<p>PM Forum members were asked to complete our annual online survey of marketing salaries. Overall the results confirm that salaries show modest growth after a year of flat lining.</p>', NULL, NULL, 0, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(55, 17, 3, 1, 1, 1, 'BlogEntry', '2015-01-29 14:41:40', '2015-01-29 14:56:35', 'and-the-winner-is', 'And the winner is…', NULL, '<p>The winners for the 2014 MPF awards were celebrated at a gala dinner on 12 March in London.</p>', NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(56, 18, 5, 1, 1, 1, 'BlogEntry', '2015-01-29 14:44:44', '2015-01-29 14:57:58', 'new-rds-appointed', 'New RDs appointed', NULL, '<p><img class="leftAlone grid-list-img; grid-list-img" title="" src="assets/Uploads/latest-news-img.jpg" alt="latest news img" width="120" height="104">Sue Carr, Director of Midlands marketing consultancy V Formation, has been appointed Regional Director for the PM Forum in the East Midlands while Jo Miners of Stonehouse Marketing has taken over in West Midlands.</p>', NULL, NULL, 0, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 13),
(57, 1, 3, 1, 1, 1, 'HomePage', '2015-01-23 14:48:25', '2015-01-29 17:45:11', 'home', 'Home', NULL, '<p>Nullam nec felis in tellus posuere egestas eget volutpat mauris. In hac habitasse platea dictumst. Nullam at ipsum tellus. Vivamus lobortis gravida nisi ac suscipit. In nibh velit, elementum a elementum convallis, dignissim quis mauris. Nam malesuada, mi et viverra cursus, lorem arcu tincidunt mauris, non aliquam sem est ut elit. Praesent a mi vel ligula aliquet sagittis.</p>\n<ul class="about-post-list"><li><a href="#">Membership rates</a></li>\n<li><a href="#">How can you Benefit?</a></li>\n<li><a href="#">FAQ’s</a></li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(58, 8, 6, 1, 1, 1, 'LocationHolderPage', '2015-01-27 14:45:09', '2015-02-02 20:23:00', 'locations', 'Locations', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(59, 19, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:42:21', '2015-02-02 20:42:21', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(60, 19, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:42:21', '2015-02-02 20:42:50', 'east-midlands', 'East Midlands', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(61, 20, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:48:10', '2015-02-02 20:48:10', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(62, 20, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:48:10', '2015-02-02 20:48:17', 'ireland', 'Ireland', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(63, 21, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:48:29', '2015-02-02 20:48:29', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(64, 21, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:48:29', '2015-02-02 20:48:34', 'london', 'London', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(65, 22, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:48:41', '2015-02-02 20:48:41', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(66, 22, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:48:41', '2015-02-02 20:48:52', 'north-west', 'North West', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(67, 23, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:48:56', '2015-02-02 20:48:56', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(68, 23, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:48:56', '2015-02-02 20:49:19', 'scotland', 'Scotland', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(69, 24, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:49:44', '2015-02-02 20:49:44', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(70, 24, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:49:44', '2015-02-02 20:49:50', 'south-west', 'South West', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(71, 25, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:50:30', '2015-02-02 20:50:30', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(72, 25, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:50:30', '2015-02-02 20:50:36', 'west-midlands', 'West Midlands', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(73, 26, 1, 0, 1, 0, 'LocationPage', '2015-02-02 20:50:55', '2015-02-02 20:50:55', 'new-location-page', 'New Location Page', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(74, 26, 2, 1, 1, 1, 'LocationPage', '2015-02-02 20:50:55', '2015-02-02 20:51:00', 'yorkshire', 'Yorkshire', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(75, 8, 7, 1, 1, 1, 'LocationHolderPage', '2015-01-27 14:45:09', '2015-02-02 20:54:38', 'locations', 'Locations', NULL, '<p>If you would like to receive regular invites to events, please <a href="#">register</a> online.</p>\n<p><strong>Benefits of corporate membership are:</strong></p>\n<ul class="list-benifits"><li>Multiple free subscriptions to <a href="#">PM magazine</a> (10 issues per year)</li>\n<li>Free educational <a href="#">core events</a> with networking opportunities nationally and internationally</li>\n<li>Invitations to member only <a href="#">training workshops</a> and the <a href="#">annual conference</a></li>\n<li>Full access to the <a href="#">article bank</a> with 4,000 keyworded articles</li>\n<li>Free regular <a href="#">research surveys</a></li>\n<li>LawNet members are entitled to 25% discount on PMF membership fees</li>\n<li>Events recognised by CIM for CPD purposes.</li>\n<li>Preferential rate for <a href="#">research surveys</a> at <a href="#">jobs.pmforumglobal.com</a></li>\n<li><a href="#">PMF Advice</a> to avoid hours of research or dead ends.</li>\n<li>Free PM Forum membership for <a href="#">redundant marketers</a></li>\n</ul>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(76, 19, 3, 1, 1, 1, 'LocationPage', '2015-02-02 20:42:21', '2015-02-02 20:57:02', 'east-midlands', 'East Midlands', NULL, '<p>The PM Forum is a 4,500 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The Scotland regional events take place in Edinburgh and Glasgow city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. Sign up here! If you are interested in hosting an event please contact Sophie Hayes.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(77, 19, 4, 1, 1, 1, 'LocationPage', '2015-02-02 20:42:21', '2015-02-02 20:57:45', 'east-midlands', 'East Midlands', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(78, 23, 3, 1, 1, 1, 'LocationPage', '2015-02-02 20:48:56', '2015-02-02 20:58:34', 'scotland', 'Scotland', NULL, '<p>The PM Forum is a 4,500 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The Scotland regional events take place in Edinburgh and Glasgow city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. Sign up here! If you are interested in hosting an event please contact Sophie Hayes.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(79, 19, 5, 1, 1, 1, 'LocationPage', '2015-02-02 20:42:21', '2015-02-03 14:42:26', 'east-midlands', 'East Midlands', NULL, '<p>The PM Forum is a 4,000 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.<br>The East Midlands regional events take place in Nottingham city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. Sign up here! If you are interested in hosting an event please contact your Regional Director Sue Carr.<br>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.<br>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(80, 19, 6, 1, 1, 1, 'LocationPage', '2015-02-02 20:42:21', '2015-02-03 14:43:52', 'east-midlands', 'East Midlands', NULL, '<p>The <strong class="committee">PM Forum</strong> is a 4,000 strong regionally-based professional body, dedicated to raising the standards of professional services marketing and enhancing the credibility of marketers working in professional service firms worldwide.</p>\n<p>The East Midlands regional <a href="#sec_Events">events</a> take place in <strong>Nottingham</strong> city centre at various venues. Membership to the PM Forum is corporate so attendance at all core events is free to all staff of member firms. <a href="about/membership.aspx">Sign up here</a>! If you are interested in hosting an event please contact your Regional Director <a href="mailto:sue@vformation.biz"><strong class="committee">Sue Carr</strong></a>.</p>\n<p>We attract a diverse audience covering marketers in a professional services firm including accountancy, law, property and management consultants. We also welcome partners and senior fee-earners with a marketing remit and agencies/suppliers to the sector.</p>\n<p>Educational events with networking opportunities are planned for each year and topics are taken from the annual member research and current trends.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 8),
(81, 13, 2, 0, 1, 0, 'BlogHolder', '2015-01-29 14:05:13', '2015-02-03 15:38:39', 'blog', 'Blog', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(82, 13, 3, 1, 1, 1, 'BlogHolder', '2015-01-29 14:05:13', '2015-02-03 15:38:43', 'blog', 'Blog', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(83, 13, 4, 1, 1, 1, 'BlogHolder', '2015-01-29 14:05:13', '2015-02-03 15:39:01', 'blog', 'Blog', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(84, 13, 5, 1, 1, 1, 'BlogHolder', '2015-01-29 14:05:13', '2015-02-03 15:39:12', 'blog', 'Blog', NULL, NULL, NULL, NULL, 0, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(85, 12, 4, 1, 1, 1, 'TrainingPage', '2015-01-27 14:51:17', '2015-02-03 17:40:51', 'training', 'Training', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_ViewerGroups`
--

CREATE TABLE IF NOT EXISTS `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TrainingWorkshop`
--

CREATE TABLE IF NOT EXISTS `TrainingWorkshop` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('TrainingWorkshop') DEFAULT 'TrainingWorkshop',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(1000) DEFAULT NULL,
  `DateStart` datetime DEFAULT NULL,
  `DateEnds` datetime DEFAULT NULL,
  `Level` enum('Beginners','Intermediate','Advanced') DEFAULT 'Beginners',
  `Duration` enum('Half Day','Full Day') DEFAULT 'Half Day',
  `Description` mediumtext,
  `BookingFormID` int(11) NOT NULL DEFAULT '0',
  `TrainingPageID` int(11) NOT NULL DEFAULT '0',
  `Location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingFormID` (`BookingFormID`),
  KEY `TrainingPageID` (`TrainingPageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `TrainingWorkshop`
--

INSERT INTO `TrainingWorkshop` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `DateStart`, `DateEnds`, `Level`, `Duration`, `Description`, `BookingFormID`, `TrainingPageID`, `Location`) VALUES
(1, 'TrainingWorkshop', '2015-02-03 17:57:16', '2015-02-03 18:22:21', 'Project and campaign management in marketing', '2015-02-25 09:00:00', '2015-02-25 12:30:00', 'Intermediate', 'Half Day', '<p>Whilst the CIM introduced the project management in marketing diploma in 2009, it removed it during the syllabus revision in 2014. However, project management remains an important topic for markets and for professional service firms where it is increasingly used to manage large client assignments and to assist in pricing strategies. This half day course reviews the main components of the project management course and considers how they apply to campaign management in the professions. Delegates are encouraged to use their own projects during the exercises.</p>', 12, 12, 'London');

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage`
--

CREATE TABLE IF NOT EXISTS `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage_Live`
--

CREATE TABLE IF NOT EXISTS `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage_versions`
--

CREATE TABLE IF NOT EXISTS `VirtualPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
