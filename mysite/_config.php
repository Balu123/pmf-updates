<?php

global $project;
$project = 'mysite';

global $databaseConfig;

if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'pmf') {

	$databaseConfig = array(
		"type" => 'MySQLDatabase',
		"server" => 'localhost',
		"username" => 'root',
		"password" => '',
		"database" => 'pmf',
		"path" => '',
	);

}  else {
	$databaseConfig = array(
		"type" => 'MySQLDatabase',
		"server" => 'mpm-shared.cs9lhvrqujq2.eu-west-1.rds.amazonaws.com',
		"username" => 'pmf',
		"password" => 'HSYU7EKYcdLXGRCq',
		"database" => 'pmf',
		"path" => '',
	);
}
Director::set_environment_type("dev");
// Set the site locale
i18n::set_locale('en_US');

Email::setAdminEmail("vikas@moonpeakmedia.com");

Security::setDefaultAdmin('admin', 'password');

HtmlEditorConfig::get('cms')->setOption(
	'theme_advanced_styles',
	'Post Image=grid-list-img,
	About Post List=about-post-list,
	List Benifits = list-benifits,
	Align Right=alignright,
	List About=list-about ,
	Map Image=map-image,
	Notice Box = notice-box'
	);

ShortcodeParser::get('default')->register('current_issue_image', array('Page', 'CurrentIssueImageShortCodeMethod'));

FulltextSearchable::enable();

Object::add_extension('Event', "FulltextSearchable('Title')");
Object::add_extension('MagazineArticle', "FulltextSearchable('Title,Content,AuthorsString,AuthorsSubject,Authors,BookAuthor')");