<?php
class Page extends SiteTree {

	private static $db = array(
		'MetaTitle'		=> 'Varchar',
		'MetaKeywords'	=> 'Text',
		'BannerContent'	=> "HTMLText",
		'PageHeading'	=> "Varchar(100)",
		"PageWithoutSidebar" => "Boolean"
		);

	private static $has_one = array(
		'BannerImage'	=> 'Image'
		);

	private static $casting = array(
        'CurrentIssueImageShortCodeMethod' => 'HTMLText'
    );

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "BannerContent");
		$fields->removeFieldFromTab("Root.Main", "BannerImage");

		$fields->insertBefore(new TextField('PageHeading', 'Page Heading'), 'MenuTitle');

		$fields->insertBefore(new TextField('Sort', 'Sort'), 'MenuTitle');

		$fields->insertBefore(new CheckboxField('PageWithoutSidebar', 'Hide Sidebar ?'), 'Content');

		$uploadField = new UploadField("BannerImage", "Banner Image");
		$uploadField->setFolderName('Uploads/banners');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");
		$fields->addFieldToTab("Root.Banner",$uploadField );

		$fields->addFieldToTab("Root.Banner", new HtmlEditorField("BannerContent", "Banner Content"));

		$fields->removeByName('Metadata');

		$fields->addFieldToTab("Root.Main", ToggleCompositeField::create('Metadata', _t('SiteTree.MetadataToggle', 'Metadata'),
			array(
				$isGetMetaFromContent = new CheckboxField('GenerateMetaData', 
					_t('MetaManager.GENERATEMETADATA','Generate Meta-data automatically from the page content')),
				$metaTitle = new TextField("MetaTitle", _t('SiteTree.MetaTitle', 'Título')),
				$metaKeyword = new TextareaField("MetaKeywords", _t('SiteTree.MetaKeywords', 'Palavras-chave')),
				$metaFieldDesc = new TextareaField("MetaDescription", $this->fieldLabel('MetaDescription')),
				$metaFieldExtra = new TextareaField("ExtraMeta",$this->fieldLabel('ExtraMeta'))
				)
			)->setHeadingLevel(4)
		);

		$metaFieldDesc
		->setRightTitle(
			_t(
				'SiteTree.METADESCHELP', 
				"Search engines use this content for displaying search results (although it will not influence their ranking)."
				)
			)
		->addExtraClass('help');

		$metaFieldExtra
		->setRightTitle(
			_t(
				'SiteTree.METAEXTRAHELP', 
				"HTML tags for additional meta information. For example &lt;meta name=\"customName\" content=\"your custom content here\" /&gt;"
				)
			)
		->addExtraClass('help');

		return $fields;
	}

	public function MetaTags($includeTitle = true) {
		$tags = parent::MetaTags(false);

		if($this->MetaKeywords){
			$tags .= "<meta name=\"keywords\" content=\"$this->MetaKeywords\">\n";
		}

		return $tags;
  	}

	public static function CurrentIssueImageShortCodeMethod() {
       	$CurrentIssueImage = MagazineIssue::get()->first()->CoverImage();
       	$strLinkCurrentIssueImage = '';
       	
       	if(is_object($CurrentIssueImage) && $CurrentIssueImage->ID) {
       		$strLinkCurrentIssueImage = '<img src="'. $CurrentIssueImage->Link() . '" width="' .  $CurrentIssueImage->getWidth(). '" height="' . $CurrentIssueImage->getHeight(). '" class="leftAlone">';
       	}
		return $strLinkCurrentIssueImage;
    }
}

class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     "action", // anyone can access this action
	 *     "action" => true, // same as above
	 *     "action" => "ADMIN", // you must have ADMIN permissions to access this action
	 *     "action" => "->checkAction" // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array ('SearchForm', 'results' );

	public function init() {
		parent::init();
		
		$themeDir = "themes/" . SSViewer::current_theme();

		$members = Member::get();

		foreach ($members as $member) {
			$strAddress = $member->AddressLine1;

			if($member->AddressLine2) {
				$strAddress .= ', ' . $member->AddressLine2;
			}
			$member->Address = $strAddress;
			
			$member->Write();
		}

		Requirements::combine_files(
			"pmf.js",
			array(
				"$themeDir/javascript/jquery.js",
				"$themeDir/javascript/bootstrap.min.js",
				"$themeDir/javascript/jquery.fs.naver.min.js",
				"$themeDir/javascript/custom.js"
				)
			);	
	}

	public function ContactPage() {
		return ContactPage::get()->first();
	}

	public function LatestBlogs() {
		return BlogEntry::get()->sort('Date', 'DESC')->limit(3);
	}

	public function BlogHolderPage() {
		return BlogHolder::get()->first();
	}

	public function UpcomingEvents() {
		$curDate = date("Y-m-d");

		$dlUpcomingEvents = Event::get()->filter('Date:GreaterThan', $curDate);
		return $dlUpcomingEvents;
	}

	public function EventsPage() {
		return EventsPage::get()->First();
	}

	public function UpcomingTrainingWorkshops() {
		$curDate = date("Y-m-d");
		$dlUpcomingTrainingWorkshops = TrainingWorkshop::get()->filter('DateStart:GreaterThan', $curDate);
		return $dlUpcomingTrainingWorkshops;
	}

	public function TrainingPage() {
		return TrainingPage::get()->First();
	}

	public function MagazinePage() {
		return MagazinePage::get()->first();
	}

	public function ArticleBankPage() {
		return MagizineArticleBankPage::get()->first();
	}

	public function BookReviewsPage() {
		return BookReviewsPage::get()->first();
	}

	public function SnapshotPages() {
		return SnapshotPage::get()->sort('MenuTitle', 'ASC');
	}

	public function SearchForm() {
		$searchText =  _t('SearchForm.SEARCH', 'Search');

		if($this->owner->request && $this->owner->request->getVar('Search')) {
			$searchText = $this->owner->request->getVar('Search');
		}

		$searchField = TextField::Create('Search')->setTitle('Search')->addExtraClass('form-control');

		if($searchText && $searchText != 'Search') {
			$searchField->setValue($searchText);
		}
		
		$fields = new FieldList($searchField);

		$actions = new FieldList(
			new FormAction('results', _t('SearchForm.GO', 'Go'))
			);

		$form = new SearchForm($this, 'SearchForm', $fields, $actions);
		$form->addExtraClass('form-inline');

		
		$form->classesToSearch(array('SiteTree'));
		return $form;
	}

	public function results($data, $form, $request) {	
		$keyword = trim($request->requestVar('Search'));
		$keyword = Convert::raw2sql($keyword);
		$keywordHTML = htmlentities($keyword, ENT_NOQUOTES, 'UTF-8');    

		$results = new ArrayList();

		$mode = ' IN BOOLEAN MODE';


		$siteTreeClasses = array('Page'); 
		$siteTreeMatch = "MATCH( Title, MenuTitle, Content) AGAINST ('$keyword'$mode)
		+ MATCH( Title, MenuTitle, Content ) AGAINST ('$keywordHTML'$mode)";

		$eventItemMatch = "MATCH( Title) AGAINST ('$keyword'$mode)
		+ MATCH( Title) AGAINST ('$keywordHTML'$mode)";

		$articleItemMatch = "MATCH( Title,AuthorsSubject ) AGAINST ('$keyword'$mode)
		+ MATCH( Title,AuthorsSubject ) AGAINST ('$keywordHTML'$mode)";


		foreach ( $siteTreeClasses as $c ) {
			$query = DataList::create($c)->where($siteTreeMatch);
			$query = $query->dataQuery()->query();
			$query->addSelect(array('Relevance' => $siteTreeMatch));

			$records = DB::query($query->sql());
			$objects = array();
			foreach( $records as $record )
			{
				if ( in_array($record['ClassName'], $siteTreeClasses) )
					$objects[] = new $record['ClassName']($record);
			}
			$results->merge($objects);
		}

		$query = DataList::create('Event')->where($eventItemMatch);
		$query = $query->dataQuery()->query();
		$query->addSelect(array('Relevance' => $eventItemMatch));

		$records = DB::query($query->sql());
		$objects = array();
		foreach( $records as $record ) $objects[] = new $record['ClassName']($record);
		$results->merge($objects);

		$query = DataList::create('MagazineArticle')->where($articleItemMatch);
		$query = $query->dataQuery()->query();
		$query->addSelect(array('Relevance' => $articleItemMatch));

		$records = DB::query($query->sql());
		$objects = array();
		foreach( $records as $record ) $objects[] = new $record['ClassName']($record);
		$results->merge($objects);

		$results->sort(array(
			'Relevance' => 'DESC',
			'Title' => 'ASC'
			));

		$results = new PaginatedList($results, $this->request);
		$results->setPageLength($this->ShowNumberOfItems());

		$data = array(
			'Results' => $results,
			'Query' => $form->getSearchQuery(),
			'Title' => _t('SearchForm.SearchResults', 'Search Results')
			);

		return $this->customise($data)->renderWith(array('Page_results', 'Page'));
	}

	public function SearchViewNoOfItemsLink(){
		return  $this->request->getURL(true) . '&show=';
	}

	public function ShowNumberOfItems(){
		return  isset($_REQUEST['show']) ? $_REQUEST['show'] : 24;
	}

	public function TestimonialWidget($noOfItems = 5) {
		$widget = new TestimonialWidget();
		$widget->NumberToShow = $noOfItems;
		return $widget->renderWith("TestimonialWidget");
	}

	public function Year() {
		return date('Y');
	}

	public function getGroupedSurveysList() {
        return GroupedList::create($dlSnapShotPages = SnapshotPage::get()->sort('Title'));
    }
}
