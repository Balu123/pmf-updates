<?php
class MagazineArticleCsvBulkLoader extends CsvBulkLoader {

	public $columnMap = array(
		"Title"      => "Title",
		"RestrictedAccess" => "->importRestrictedAccess",
		"PublicationDate" => "PublicationDate",
		"BookAuthor"   => "BookAuthor",
		"EventSpeaker" => "EventSpeaker",
		"Summary"   => "Summary",
		"Content"   => "Content",
		"Authors" => "Authors",
		"AuthorsString" => "AuthorsString",
		"ArticleSubject" => "ArticleSubject",
		"MagazineIssueTitle" => 'MagazineIssue.Title',
		"ArticleSource" => 'ArticleSource.Title',
		"ArticleSection" => 'ArticleSection.Title',
		"ArticleKeywords" => '->importArticleKeywords',
		"ArticleChannels" => '->importArticleChannels'
		);

	public $relationCallbacks = array(

		'MagazineIssue.Title' => array(
			'relationname' => 'MagazineIssue',
			'callback' => 'getMagazineIssueByTitle'
			),

		'ArticleSource.Title' => array(
			'relationname' => 'ArticleSource',
			'callback' => 'getArticleSourceByTitle'
			),

		'ArticleSection.Title' => array(
			'relationname' => 'ArticleSection',
			'callback' => 'getArticleSectionByTitle'
			)
		);

	public static function importRestrictedAccess(&$obj, $val, $record) {
		$val = trim($val);

		if($val == 'Yes') {
			$obj->RestrictedAccess = 1;
		} else {
			$obj->RestrictedAccess = 0;
		}
	}

	public static function getArticleSourceByTitle(&$obj, $val, $record) {
		$strSourceTitle = trim($val);

		return ArticleSource::get()->filter('Title', $strSourceTitle)->first();
	}

	public static function getMagazineIssueByTitle(&$obj, $val, $record) {

		$doRet = '';

		$strMagazineIssueTitle = trim($record['MagazineIssue.Title']);
		$strMagazineIssueYear = trim($record['MagazineIssueYear']);
		$strMagazineIssueVolume = trim($record['MagazineIssueVolume']);
		$strMagazineIssueIssue = trim($record['MagazineIssueIssue']);

		$doMagazineIssue = MagazineIssue::get()->filter(array(
			'Title' =>  $strMagazineIssueTitle,
			'Year' => $strMagazineIssueYear,
			'Volume' =>  $strMagazineIssueVolume,
			'Issue' => $strMagazineIssueIssue
			))->first();

		if(is_object($doMagazineIssue) && $doMagazineIssue->ID) {
			$doRet = $doMagazineIssue;
		} else {
			$doMagazineIssue = new MagazineIssue();
			$doMagazineIssue->Title = $strMagazineIssueTitle;
			$doMagazineIssue->Year =  $strMagazineIssueYear;
			$doMagazineIssue->Volume = $strMagazineIssueVolume;
			$doMagazineIssue->Issue = $strMagazineIssueIssue;
			$doMagazineIssue->Publish = 1;
			$doMagazineIssue->SourceID = $obj->ArticleSourceID;
			$doMagazineIssue->write();

			$doRet = $doMagazineIssue;
		}

		return $doRet;
	}


	public static function getArticleSectionByTitle(&$obj, $val, $record) {

		$doRet = '';
		$strSectionTitle = trim($val);

		$doArticleSection =  ArticleSection::get()->filter('Title',  $strSectionTitle)->first();

		if(is_object($doArticleSection) && $doArticleSection->ID) {
			$doRet = $doArticleSection;
		} else {
			$doArticleSection = new ArticleSection();
			$doArticleSection->Title =  $strSectionTitle;
			$doArticleSection->write();

			$doRet = $doArticleSection;
		}

		return $doRet;
	}

	public static function importArticleKeywords(&$obj, $val, $record) {
		$arrKeywords = explode(',', $val);

		if(count($arrKeywords)) foreach ($arrKeywords as $strKeyword) {
			$strKeyword = trim($strKeyword);

			if ($strKeyword) {
				$doKeyword = ArticleKeyword::get()->filter('Title', $strKeyword)->first();

				if(is_object($doKeyword) && $doKeyword->ID) {
					$obj->ArticleKeywords()->Add($doKeyword);
				} else {

					$doKeyword = new ArticleKeyword();
					$doKeyword->Title = $strKeyword;
					$doKeyword->write();

					$obj->ArticleKeywords()->Add($doKeyword);
				}
			}
		}
	}

	public static function importArticleChannels(&$obj, $val, $record) {
		$arrChannels = explode(',', $val);

		foreach ($arrChannels as $strChannel) {

			if(strpos($strChannel, 'career_guidance') !== false) {

				$doArticleChannel = ArticleChannel::get()->filter('Title', 'Career Guidance')->first();

				$obj->ArticleChannels()->Add($doArticleChannel);

			} elseif (strpos($strChannel, 'book_reviews') !== false) {

				$doArticleChannel = ArticleChannel::get()->filter('Title', 'Book Review')->first();

				$obj->ArticleChannels()->Add($doArticleChannel);

			} else {

				$doArticleChannel = ArticleChannel::get()->filter('Title', 'Article Bank')->first();
				
				$obj->ArticleChannels()->Add($doArticleChannel);
			}
		}

	}
}