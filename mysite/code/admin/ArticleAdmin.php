<?php

class ArticleAdmin extends ModelAdmin{

	public static $managed_models = array(
		'MagazineArticle',
		'ArticleChannel',
		'MagazineIssue',
		'ArticleKeyword',
		'ArticleKeywordGroup',
		'ArticleSource',
		'ArticleSection',
		'Book'
   	);

	private static $model_importers = array(
		'MagazineArticle' => 'MagazineArticleCsvBulkLoader'
		);
	
  	static $url_segment = 'articleadmin';
  	static $menu_title = 'Articles';
}