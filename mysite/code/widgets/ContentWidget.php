<?php

class ContentWidget extends Widget{
	
	static $db = array(
		"Title" => "Varchar",
		"CustomCSSClass" => "Varchar",
		"Content" => "HTMLText"
	);

	private static $title = null; //don't show a title for this widget by default
	private static $cmsTitle = "Content";
	private static $description = "Add a section of content to sidebar of your page.";

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName("Title");
		$fields->removeByName("CustomCSSClass");
		$fields->removeByName("Content");

		$field = DropdownField::create('CustomCSSClass', 'Custom CSS Class', array('list-custom' => 'list-custom', 'general-content' => 'general-content'))->setEmptyString('(Select one)');
        $fields->push($field);

		$fields->push(HtmlEditorField::create("Content","Content")->setRows(5));
		return $fields;
	}

	public function Title(){
		if($this->Title){
			return $this->Title;
		}
	}

	public function getHTMLContent() {
		return ShortcodeParser::get_active()->parse($this->Content);
	}

	public function getCustomCSSClassContent() {
		return $this->CustomCSSClass;
	}
}