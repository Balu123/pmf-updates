<?php


class TestimonialWidget extends Widget {

	private static $db = array(
		"NumberToShow" => "Int"
	);

	private static $defaults = array(
		"NumberToShow" => 3,
		);

	private static $cmsTitle = "Testimonial Widget";

	private static $description = "Show Testimonials (Managed in Testimonial Page)";


	function getCMSFields() {
		$fields = parent::getCMSFields(); 

		$fields->merge(
			new FieldList(
				new NumericField("NumberToShow", "Number of Items to show")
				)
			);

		$this->extend('updateCMSFields', $fields);

		return $fields;
	}

	function Title() {
		return 'Testimonial Widget';
	}

	function getTestimonialItems() {
		return Testimonial::get()->Sort("Rand()")->limit($this->NumberToShow);
	}
}