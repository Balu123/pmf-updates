<?php
class ConferencePage extends Page {

	private static $db = array(
		'SidebarContent1' => 'HTMLText',
		'SidebarContent2' => 'HTMLText',
		'SidebarContent3' => 'HTMLText',
	);

	private static $has_one = array(
	);

	public static $has_many = array(
		'Sponsors'	=> 'Sponsor'
	);

	public function canCreate($member = null){
		return !ConferencePage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->addFieldToTab("Root.Sidebar", new HtmlEditorField("SidebarContent1", "Sidebar Content 1"));
		$fields->addFieldToTab("Root.Sidebar", new HtmlEditorField("SidebarContent2", "Sidebar Content 2"));
		$fields->addFieldToTab("Root.Sidebar", new HtmlEditorField("SidebarContent3", "Sidebar Content 3"));

		$cfgSponsors = new GridFieldConfig_RelationEditor();
		$cfgSponsors->removeComponent($cfgSponsors->getComponentByType('GridFieldDeleteAction'));
		$cfgSponsors->removeComponent($cfgSponsors->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgSponsors->addComponent(new GridFieldBulkUpload());
		$cfgSponsors->addComponent(new GridFieldDeleteAction());
		$cfgSponsors->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.Sponsors", new GridField("Sponsors", "Sponsors", $this->Sponsors(),$cfgSponsors ));
	  	
		return $fields;
	}

}

class ConferencePage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

}
