<?php

class Sponsor extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar",
		"Link"		=> "Varchar(1000)",
		"SortOrder"	=> "Int"
		);

	public static $has_one = array(
		"ConferencePage"	=> "ConferencePage",
		"Image"				=> "Image"
		);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array ("Title");

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "ConferencePageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
		return $fields;
	}
}