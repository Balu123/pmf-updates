<?php
class ContactPage extends Page {

	private static $db = array(
		"CompanyDetails" => "HTMLText",
		"ShowContactUsForm" => "Boolean"
	);

	private static $has_one = array(
	);

	private static $has_many = array(
		'ContactUsFormSubmissions' => 'ContactUsFormSubmission',
		'StaffMembers'			=> 'PMFStaffMembers'
	);

	public function canCreate($member = null){
		return !ContactPage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName("Widgets");

		$fields->insertBefore(new CheckboxField('ShowContactUsForm', 'Show Contact Form'), 'Content');

		$CompanyDetailsField = new HTMLEditorField('CompanyDetails', 'Company Details');
		$CompanyDetailsField->setRows(5);
		$fields->insertBefore($CompanyDetailsField, 'Content');

		$cfgStaffMembers = new GridFieldConfig_RelationEditor();
		$cfgStaffMembers->removeComponent($cfgStaffMembers->getComponentByType('GridFieldDeleteAction'));
		$cfgStaffMembers->removeComponent($cfgStaffMembers->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgStaffMembers->addComponent(new GridFieldDeleteAction());
		$cfgStaffMembers->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.StaffMembers", new GridField("StaffMembers", "", $this->StaffMembers(),$cfgStaffMembers ));

		if($this->ShowContactUsForm) {
			$cfgContactUsFormSubmissions = new GridFieldConfig_RelationEditor();
			$cfgContactUsFormSubmissions->removeComponent($cfgContactUsFormSubmissions->getComponentByType('GridFieldDeleteAction'));
			$cfgContactUsFormSubmissions->removeComponent($cfgContactUsFormSubmissions->getComponentByType('GridFieldAddNewButton'));
			$cfgContactUsFormSubmissions->removeComponent($cfgContactUsFormSubmissions->getComponentByType('GridFieldAddExistingAutocompleter'));
			$fields->addFieldToTab("Root.ContactUsFormSubmissions", new GridField("ContactUsFormSubmissions", "", $this->ContactUsFormSubmissions(),$cfgContactUsFormSubmissions ));
		}
		
		return $fields;
	}


}
class ContactPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		'ContactForm'
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	// Contact us Form 
	public function ContactForm() {
        // Create fields
        $fields = new FieldList(
            new TextField('Name'),
           	new EmailField('Email'),
            new TextareaField('Message')
        );

        // Create actions
       	$actions = new FieldList( 
            new FormAction('submit', 'Submit') 
        ); 

        $validator = new RequiredFields('Name', 'Message');

        return new Form($this, 'ContactForm', $fields, $actions, $validator); 

    }

    public function submit($data, $form) { 

       	$submission = new ContactUsFormSubmission();
        $form->saveInto($submission);
        $submission->ContactPageID = $this->ID;
        $submission->write();
        return  array(
            'Content' => '<p>Thank you for your feedback.</p>',
            'ContactForm' => ''
        );

    }

}
