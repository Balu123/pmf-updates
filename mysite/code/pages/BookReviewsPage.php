<?php
class BookReviewsPage extends Page {

	private static $db = array(
		);

	private static $has_one = array(
		);

	public static $has_many = array(
		"Books"	=> "Book"
		);

	public function canCreate($member = null){
		return !BookReviewsPage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$cfgBooks = new GridFieldConfig_RelationEditor();
		$cfgBooks->removeComponent($cfgBooks->getComponentByType('GridFieldDeleteAction'));
		$cfgBooks->removeComponent($cfgBooks->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgBooks->addComponent(new GridFieldDeleteAction());
		$fields->addFieldToTab("Root.Books", new GridField("Books", "Books", $this->Books(),$cfgBooks ));
		
		return $fields;
	}

}
class BookReviewsPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		'QuickReviewSearchForm',
		'doQuickSearch',
		'AdvanceReviewSearchForm',
		'doAdvanceSearch'
		);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function PaginatedBooks() {
		$dlBooks = $this->Books()->Exclude('BookOfTheMonth', 1);
		$dlPaginatedBooks = new PaginatedList($dlBooks, $this->request);
		$dlPaginatedBooks->setPageLength($this->ShowNumberOfItems());
		
		return $dlPaginatedBooks;
	}

	public function SearchViewNoOfItemsLink(){
		return  $this->request->getURL(true) . '&show=';
	}

	public function ShowNumberOfItems(){
		return  isset($_REQUEST['show']) ? $_REQUEST['show'] : 24;
	}

	public function QuickReviewSearchForm() {

		$arrMatchOptions = array('All of the words', 'Any of the words', 'The exact phrase');
		$arrSortByOptions = array('A - Z', 'Publication date');

		$strSearchInputValue = '';
		$strMatchWithValue = 0;
		$strSortByValue = 0;

		if(array_key_exists('SearchInput', $_REQUEST)) {
			$strSearchInputValue = $_REQUEST['SearchInput'];
		}

		if(array_key_exists('MatchWith', $_REQUEST)) {
			$strMatchWithValue = (int) $_REQUEST['MatchWith'];
		}

		if(array_key_exists('SortBy', $_REQUEST)) {
			$strSortByValue = $_REQUEST['SortBy'];
		}

		$fields = new FieldList(
			
			TextField::Create('SearchInput')->SetTitle('Search for')->setValue($strSearchInputValue)->setAttribute('placeholder', "Search for")->addExtraClass('form-control'),
			
			OptionsetField::Create('MatchWith')->SetTitle('')->setValue($strMatchWithValue)->setSource($arrMatchOptions)->setTemplate('PMFOptionsetField'),

			OptionsetField::Create('SortBy')->SetTitle('Sort by')->setValue($strSortByValue)->setSource($arrSortByOptions)->setTemplate('PMFOptionsetField')
		);

		$actions = new FieldList(
			new FormAction('doQuickSearch', 'Search')
			);

		$validator = new RequiredFields('SearchInput');

		$form =  new Form($this, 'QuickReviewSearchForm', $fields, $actions, $validator);

		$form->setTemplate('QuickReviewSearchForm');
		$form->disableSecurityToken();
		$form->setFormMethod('GET');

		return $form;
	}

	public function doQuickSearch($data,$form,$request) {

		$results = new ArrayList();
		$mode = ' IN BOOLEAN MODE';

		$strSerchQuery = convert::raw2sql($request->requestVar('SearchInput'));
		$iMatchWith = convert::raw2sql($request->requestVar('MatchWith'));
		$iSortBy = convert::raw2sql($request->requestVar('SortBy'));

		$arrSearchQuery = explode(' ', $strSerchQuery);
		//Build query for MatchWith 

		if($iMatchWith == 0) { // All of the words
			$strSerchQueryContacted = '';
			foreach ($arrSearchQuery as $value) {
				$strSerchQueryContacted .= ' +'. $value;
			}
			$articleItemMatch = " MATCH(Title, BookAuthor, Content) AGAINST ('$strSerchQueryContacted'$mode)";
		} elseif ($iMatchWith == 1) { // Any of the words 
			$articleItemMatch = " MATCH(Title, BookAuthor, Content) AGAINST ('$strSerchQuery'$mode)";
		} else { // the exact match
			$articleItemMatch = " MATCH(Title, BookAuthor, Content) AGAINST ('\"$strSerchQuery\"'$mode)";
		}

		$query = DataList::create('MagazineArticle')->where($articleItemMatch);
		$query = $query->dataQuery()->query();
		$query->addSelect(array('Relevance' => $articleItemMatch));

		$records = DB::query($query->sql());
		$objects = array();
		foreach( $records as $record ) {
			$object = new $record['ClassName']($record);
			foreach ($object->ArticleChannels() as $Channel) {
				if($Channel->Title == 'Book Review') {
					 $objects[] = $object;
					 break;
				}
			}
		}
		$results->merge($objects);

		if($iSortBy == 1) {
			$results->sort(array(
				'Relevance' => 'DESC',
				'PublicationDate' => 'DESC'
				));
		} else {
			$results->sort(array(
				'Relevance' => 'DESC',
				'Title' => 'ASC'
				));
		}
		$results = new PaginatedList($results, $this->request);
		$results->setPageLength($this->ShowNumberOfItems());

		return array('SearchResult' => $results);
	}

	public function AdvanceReviewSearchForm() {

		$arrMatchOptions = array('All of the words', 'Any of the words', 'The exact phrase');
		$arrSortByOptions = array('A - Z', 'Publication date');
		$arrKeywords = ArticleKeyword::get()->filter('MagazineArticles.ArticleChannels.Title','Book Review')->sort('Title', 'ASC')->map('ID', 'Title');

		$strSearchInputValue = '';
		$strKeywordValue = '';
		$strSortByValue = 0;

		if(array_key_exists('SearchInputAdvance', $_REQUEST)) {
			$strSearchInputValue = $_REQUEST['SearchInputAdvance'];
		}

		if(array_key_exists('Keyword', $_REQUEST)) {
			$strKeywordValue =  $_REQUEST['Keyword'];
		}

		if(array_key_exists('SortByAdvance', $_REQUEST)) {
			$strSortByValue = $_REQUEST['SortByAdvance'];
		}

		$fields = new FieldList(
			
			DropdownField::create('Keyword')->SetTitle('By Keyword')->setValue($strKeywordValue)->SetEmptyString('All Keywords')->setSource($arrKeywords)->addExtraClass('form-control')->setAttribute('multiple', ''),

			TextField::Create('SearchInputAdvance')->SetTitle('By author surname')->setValue($strSearchInputValue)->setAttribute('placeholder', "By author surname")->addExtraClass('form-control'),

			OptionsetField::Create('SortByAdvance')->SetTitle('Sort by')->setValue($strSortByValue)->setSource($arrSortByOptions)->setTemplate('PMFOptionsetField')
		);

		$actions = new FieldList(
			new FormAction('doAdvanceSearch', 'Search')
			);

		$form =  new Form($this, 'AdvanceReviewSearchForm', $fields, $actions);

		$form->setTemplate('AdvanceReviewSearchForm');
		$form->disableSecurityToken();
		$form->setFormMethod('GET');

		return $form;
	}

	public function doAdvanceSearch($data,$form,$request) {
		$strSerchQuery = convert::raw2sql($request->requestVar('SearchInputAdvance'));
		$iKeywordID = convert::raw2sql($request->requestVar('Keyword'));
		$iSortBy = convert::raw2sql($request->requestVar('SortByAdvance'));


		if($iSortBy == 1) {
			$arrSortBy = array("PublicationDate" => "DESC");
		} else {
			$arrSortBy = array("Title" => "ASC");
		}

		$arrFilters = array();

		if($iKeywordID) {
			$arrFilters['ArticleKeywords.ID'] =  $iKeywordID;
		}

		if($strSerchQuery) {
			$arrFilters['Authors:PartialMatch'] =  $strSerchQuery;
		}

		$arrFilters['ArticleChannels.Title'] =  'Book Review';

		$results = MagazineArticle::get()->filter($arrFilters)->sort($arrSortBy);

		$results = new PaginatedList($results, $this->request);
		$results->setPageLength($this->ShowNumberOfItems());

		return array('SearchResult' => $results);
	}

	public function isSearchActive() {
		$strAction = $this->getRequest()->param("Action");
		if($strAction == 'QuickReviewSearchForm' || $strAction == 'AdvanceReviewSearchForm') {
			$bRet = true;
		} else {
			$bRet = false;
		}

		return $bRet;
	}
}
