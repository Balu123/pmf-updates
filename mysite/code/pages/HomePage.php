<?php
class HomePage extends Page {

	private static $db = array(
		"ContentHeading" => "Varchar"
	);

	public static $has_many = array(
		"HomeBanners"	=> "HomeBanner",
		"HomeAdvertisements" => "HomeAdvertisement"
	);

	public function canCreate($member = null){
		return !HomePage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "Content");

		$fields->addFieldToTab("Root.Content", new TextField("ContentHeading", "Heading"));
		$fields->addFieldToTab("Root.Content", new HtmlEditorField("Content", "Content"));
		$fields->removeByName("Banner");

		
		$cfgBanners = new GridFieldConfig_RelationEditor();
		$cfgBanners->removeComponent($cfgBanners->getComponentByType('GridFieldDeleteAction'));
		$cfgBanners->removeComponent($cfgBanners->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgBanners->addComponent(new GridFieldBulkUpload());
		$cfgBanners->addComponent(new GridFieldDeleteAction());
		$cfgBanners->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.HomeBanners", new GridField("HomeBanner", "Banners", $this->HomeBanners(),$cfgBanners ));
	  	

	  	$cfgAdvertisements = new GridFieldConfig_RelationEditor();
		$cfgAdvertisements->removeComponent($cfgAdvertisements->getComponentByType('GridFieldDeleteAction'));
		$cfgAdvertisements->removeComponent($cfgAdvertisements->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgAdvertisements->addComponent(new GridFieldBulkUpload());
		$cfgAdvertisements->addComponent(new GridFieldDeleteAction());
		$cfgAdvertisements->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.Advertisements", new GridField("Advertisements", "Advertisements", $this->HomeAdvertisements(),$cfgAdvertisements ));

		return $fields;
	}
}

class HomePage_Controller extends Page_Controller {

	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function Testimonials() {
		return Testimonial::get()->filter('ShowOnHomePage', 1)->sort('Rand()')->limit(1);
	}

	public function JobVacancies() {
		$alJobItems = new ArrayList();

		$completeurl = "http://jobs.pmforumglobal.com/jobsrss/?IndustrySector=31&countrycode=GB";

		$xml = simplexml_load_file($completeurl);

		foreach ($xml->channel->item as $i => $jobItem) {

			if($i > 4) {
				break;
			}

			$strJobTitle = (string) $jobItem->title;
			$strJobLink = (string) $jobItem->link;

			$adItem = new ArrayData(array('JobTitle' => $strJobTitle, 'JobLink' => $strJobLink ));

			$alJobItems->push($adItem);
			
		}
		
		return $alJobItems;
	}

}
