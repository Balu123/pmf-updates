<?php
class MagizineArticleBankPage extends Page {

	private static $db = array(
		);

	private static $has_one = array(
		);

	public function canCreate($member = null){
		return !MagizineArticleBankPage::get()->first();
	}

}
class MagizineArticleBankPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */

	private static $allowed_actions = array (
		'QuickSearchForm',
		'doQuickSearch',
		'AdvanceSearchForm',
		'doAdvanceSearch'
		);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function QuickSearchForm() {

		$arrMatchOptions = array('All of the words', 'Any of the words', 'The exact phrase');
		$arrSortByOptions = array('Popularity', 'Publication date');

		$strSearchInputValue = '';
		$strMatchWithValue = 0;
		$strSortByValue = 1;

		if(array_key_exists('SearchInput', $_REQUEST)) {
			$strSearchInputValue = $_REQUEST['SearchInput'];
		}

		if(array_key_exists('MatchWith', $_REQUEST)) {
			$strMatchWithValue = (int) $_REQUEST['MatchWith'];
		}

		if(array_key_exists('SortBy', $_REQUEST)) {
			$strSortByValue = $_REQUEST['SortBy'];
		}
	
		$fields = new FieldList(
			
			TextField::Create('SearchInput')->SetTitle('Search for')->setValue($strSearchInputValue)->setAttribute('placeholder', "Search for")->addExtraClass('form-control'),
			
			OptionsetField::Create('MatchWith')->SetTitle('')->setValue($strMatchWithValue)->setSource($arrMatchOptions)->setTemplate('PMFOptionsetField'),

			OptionsetField::Create('SortBy')->SetTitle('Sort by')->setValue($strSortByValue)->setSource($arrSortByOptions)->setTemplate('PMFOptionsetField')
			);

		$actions = new FieldList(
			new FormAction('doQuickSearch', 'Search')
			);

		$validator = new RequiredFields('SearchInput');

		$form =  new Form($this, 'QuickSearchForm', $fields, $actions, $validator);

		$form->setTemplate('QuickSearchForm');
		$form->disableSecurityToken();
		$form->setFormMethod('GET');
		$form->addExtraClass('search-form');

		return $form;
	}

	public function doQuickSearch($data,$form, $request) {
		
		$results = new ArrayList();
		$mode = ' IN BOOLEAN MODE';

		$strSerchQuery = convert::raw2sql($request->requestVar('SearchInput'));
		$iMatchWith = convert::raw2sql($request->requestVar('MatchWith'));
		$iSortBy = convert::raw2sql($request->requestVar('SortBy'));

		$arrSearchQuery = explode(' ', $strSerchQuery);
		//Build query for MatchWith 

		if($iMatchWith == 0) { // All of the words
			$strSerchQueryContacted = '';
			foreach ($arrSearchQuery as $value) {
				$strSerchQueryContacted .= ' +'. $value;
			}
			$articleItemMatch = " MATCH(Title, Content) AGAINST ('$strSerchQueryContacted'$mode)";
		} elseif ($iMatchWith == 1) { // Any of the words 
			$articleItemMatch = " MATCH(Title, Content) AGAINST ('$strSerchQuery'$mode)";
		} else { // the exact match
			$articleItemMatch = " MATCH(Title, Content) AGAINST ('\"$strSerchQuery\"'$mode)";
		}

		$query = DataList::create('MagazineArticle')->where($articleItemMatch);
		$query = $query->dataQuery()->query();
		$query->addSelect(array('Relevance' => $articleItemMatch));

		$records = DB::query($query->sql());
		$objects = array();
		foreach( $records as $record ) $objects[] = new $record['ClassName']($record);
		$results->merge($objects);

		if($iSortBy == 1) {
			$results->sort(array(
				'Relevance' => 'DESC',
				'PublicationDate' => 'DESC'
				));
		} else {
			$results->sort(array(
				'Relevance' => 'DESC',
				'Title' => 'ASC'
				));
		}
		$results = new PaginatedList($results, $this->request);
		$results->setPageLength($this->ShowNumberOfItems());

		return array('SearchResult' => $results);
	}

	public function SearchViewNoOfItemsLink(){
		return  $this->request->getURL(true) . '&show=';
	}

	public function ShowNumberOfItems(){
		return  isset($_REQUEST['show']) ? $_REQUEST['show'] : 24;
	}

	public function isSearchActive() {
		$strAction = $this->getRequest()->param("Action");
		if($strAction == 'QuickSearchForm' || $strAction == 'AdvanceSearchForm') {
			$bRet = true;
		} else {
			$bRet = false;
		}

		return $bRet;
	}

	public function AdvanceSearchForm() {

		$arrMatchOptions = array('All of the words', 'Any of the words', 'The exact phrase');
		$arrSortByOptions = array('A - Z', 'Publication date');
		$arrDates = array(1 => 'Last 3 months', 2 => 'Last 6 months', 3 => 'Last 12 months', 4 => 'Older');
		$arrKeywords = ArticleKeyword::get()->sort('Title', 'ASC')->map('ID', 'Title');

		$strSearchInputValue = '';
		$strDateValue = '';
		$strKeywordValue = '';
		$strSortByValue = 0;

		if(array_key_exists('SearchInputAdvance', $_REQUEST)) {
			$strSearchInputValue = $_REQUEST['SearchInputAdvance'];
		}

		if(array_key_exists('Keyword', $_REQUEST)) {
			$strKeywordValue =  $_REQUEST['Keyword'];
		}

		if(array_key_exists('Date', $_REQUEST)) {
			$strDateValue =  $_REQUEST['Date'];
		}

		if(array_key_exists('SortByAdvance', $_REQUEST)) {
			$strSortByValue = $_REQUEST['SortByAdvance'];
		}
	

		$fields = new FieldList(
			
			DropdownField::create('Keyword')->SetTitle('By Keyword')->setValue($strKeywordValue)->SetEmptyString('All Keywords')->setSource($arrKeywords)->addExtraClass('form-control'),

			TextField::Create('SearchInputAdvance')->SetTitle('By author surname')->setValue($strSearchInputValue)->setAttribute('placeholder', "By author surname")->addExtraClass('form-control'),

			DropdownField::create('Date')->SetTitle('By Date')->setValue($strDateValue)->SetEmptyString('Any Date')->setSource($arrDates)->addExtraClass('form-control'),

			OptionsetField::Create('SortByAdvance')->SetTitle('Sort by')->setValue($strSortByValue)->setSource($arrSortByOptions)->setTemplate('PMFOptionsetField')
			);

		$actions = new FieldList(
			new FormAction('doAdvanceSearch', 'Search')
			);

		$form =  new Form($this, 'AdvanceSearchForm', $fields, $actions);

		$form->setTemplate('AdvanceSearchForm');
		$form->disableSecurityToken();
		$form->setFormMethod('GET');

		return $form;
	}

	public function doAdvanceSearch($data,$form, $request) {

		$strSerchQuery = convert::raw2sql($request->requestVar('SearchInputAdvance'));
		$iKeywordID = convert::raw2sql($request->requestVar('Keyword'));
		$iDate = convert::raw2sql($request->requestVar('Date'));
		$iSortBy = convert::raw2sql($request->requestVar('SortByAdvance'));


		if($iDate == 1) { //last 3 months
			$date = strtotime(date('Y-m-d') .' -3 months');
			$dtOlder = date('Y-m-d', $date).' 00:00:00';
		} elseif ($iDate == 2) { // last 6 months
			$date = strtotime(date('Y-m-d') .' -6 months');
			$dtOlder = date('Y-m-d', $date).' 00:00:00';
		} elseif ($iDate == 3) { // last 12 months
			$date = strtotime(date('Y-m-d') .' -12 months');
			$dtOlder = date('Y-m-d', $date).' 00:00:00';
		} else { // older than 12 months
			$dtOlder = '1995-01-01 00:00:00';
		}

		if($iSortBy == 1) {
			$arrSortBy = array("PublicationDate" => "DESC");
		} else {
			$arrSortBy = array("Title" => "ASC");
		}

		$arrFilters = array();

		if($iKeywordID) {
			$arrFilters['ArticleKeywords.ID'] =  $iKeywordID;
		}

		if($strSerchQuery) {
			$arrFilters['Authors:PartialMatch'] =  $strSerchQuery;
		}

		$arrFilters['PublicationDate:GreaterThan'] =  $dtOlder;

		$results = MagazineArticle::get()->filter($arrFilters)->sort($arrSortBy);

		$results = new PaginatedList($results, $this->request);
		$results->setPageLength($this->ShowNumberOfItems());

		return array('SearchResult' => $results);
	}
}
