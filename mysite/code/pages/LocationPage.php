<?php
class LocationPage extends Page {

	private static $db = array(
		"SidebarContent"	=> "HTMLText"
	);

	private static $has_one = array(
	);

	public static $has_many = array(
		"Events"	=> "Event",
		"CommitteeMembers"	=> "CommitteeMember"
	);

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$SidebarContentField = new HTMLEditorField('SidebarContent', 'Previous Events Block Content');
		$fields->insertBefore($SidebarContentField, 'Content');

		$cfgEvents = new GridFieldConfig_RelationEditor();
		$cfgEvents->removeComponent($cfgEvents->getComponentByType('GridFieldDeleteAction'));
		$cfgEvents->removeComponent($cfgEvents->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgEvents->addComponent(new GridFieldDeleteAction());
		$fields->addFieldToTab("Root.Events", new GridField("Events", "Add Event", $this->Events(),$cfgEvents ));
	  	

		$cfgCommitteeMembers = new GridFieldConfig_RelationEditor();
		$cfgCommitteeMembers->removeComponent($cfgCommitteeMembers->getComponentByType('GridFieldDeleteAction'));
		$cfgCommitteeMembers->removeComponent($cfgCommitteeMembers->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgCommitteeMembers->addComponent(new GridFieldDeleteAction());
		$cfgCommitteeMembers->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.CommitteeMembers", new GridField("CommitteeMembers", "Committee Members", $this->CommitteeMembers(),$cfgCommitteeMembers ));

		return $fields;
	}

}

class LocationPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function FutureEvents($iLimit = 2) {
		$curDate = date("Y-m-d");

		$dlFutureEvents = $this->Events()->filter('Date:GreaterThan', $curDate)->limit($iLimit);

		return $dlFutureEvents;
	}

}
