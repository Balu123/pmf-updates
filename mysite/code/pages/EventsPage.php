<?php
class EventsPage extends Page {

	private static $db = array(
		"ShowCustomBlock"	=> "Boolean",
		"ShowTestimonialInCustomBlock" => "Boolean"
	);

	private static $has_one = array(
	);

	public function canCreate($member = null){
		return !EventsPage::get()->first();
	}


	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->insertBefore(new CheckboxField("ShowCustomBlock", "Show Custom Block"), 'Content');

		$fields->insertBefore(new CheckboxField("ShowTestimonialInCustomBlock", "Show Testimonial In Custom Block"), 'Content');

		return $fields;
	}
	
}

class EventsPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function MergedEventItems() {
		$dlMergedEventItems = new ArrayList();

		$curDate = date("Y-m-d");

		$dlEvents = Event::get()->filter(array('Date:GreaterThan' => $curDate))->sort('Date ASC');


		if($this->ShowTestimonialInCustomBlock) {
			$htmlTestimonial = '<ul class="list-about-sidebar">'. $this->TestimonialWidget(2)  . '</ul>';
			$doCustomBlock = new ArrayData(
				array(
					'ClassName' => 'CustomBlock',
					'Content'	=> $htmlTestimonial
				));
		} else {
			$doCustomBlock = new ArrayData(
				array(
					'ClassName' => 'CustomBlock',
					'Content'	=> ShortcodeParser::get_active()->parse($this->Content) 
				));
		}

		foreach ($dlEvents as $i => $doEvent) {
			if($i == 2) {
				$dlMergedEventItems->push($doCustomBlock);
			}
			$dlMergedEventItems->push($doEvent);	
		}

		if($dlEvents->Count() < 3) {
			$dlMergedEventItems->push($doCustomBlock);
		}
		
		return $dlMergedEventItems;
	}
}
