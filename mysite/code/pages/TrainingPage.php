<?php
class TrainingPage extends Page {

	private static $db = array(
		"ShowCustomBlock"	=> "Boolean",
		"ShowTestimonialInCustomBlock" => "Boolean"
		);

	private static $has_one = array(
		);

	public static $has_many = array(
		"TrainingWorkshops"	=> "TrainingWorkshop"
		);

	public function canCreate($member = null){
		return !TrainingPage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->insertBefore(new CheckboxField("ShowCustomBlock", "Show Custom Block"), 'Content');

		$fields->insertBefore(new CheckboxField("ShowTestimonialInCustomBlock", "Show Testimonial In Custom Block"), 'Content');

		$cfgTrainingWorkshops = new GridFieldConfig_RelationEditor();
		$cfgTrainingWorkshops->removeComponent($cfgTrainingWorkshops->getComponentByType('GridFieldDeleteAction'));
		$cfgTrainingWorkshops->removeComponent($cfgTrainingWorkshops->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgTrainingWorkshops->addComponent(new GridFieldDeleteAction());
		$fields->addFieldToTab("Root.TrainingWorkshops", new GridField("TrainingWorkshops", "Training Workshops", $this->TrainingWorkshops(),$cfgTrainingWorkshops ));

		return $fields;
	}

}

class TrainingPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function MergedTrainingWorkshops() {
		$dlMergedTrainingWorkshops = new ArrayList();

		$dlTrainingWorkshops = $this->TrainingWorkshops(2);

		if($this->ShowTestimonialInCustomBlock) {
			$htmlTestimonial = '<ul class="list-about-sidebar">'. $this->TestimonialWidget()  . '</ul>';
			$doCustomBlock = new ArrayData(
				array(
					'ClassName' => 'CustomBlock',
					'Content'	=> $htmlTestimonial
					));
		} else {
			$doCustomBlock = new ArrayData(
				array(
					'ClassName' => 'CustomBlock',
					'Content'	=> ShortcodeParser::get_active()->parse($this->Content)
					));
		}

		foreach ($dlTrainingWorkshops as $i => $doTrainingWorkshop) {
			if($i == 2) {
				$dlMergedTrainingWorkshops->push($doCustomBlock);
			}
			$dlMergedTrainingWorkshops->push($doTrainingWorkshop);
		}

		if($dlTrainingWorkshops->Count() < 3) {
			$dlMergedTrainingWorkshops->push($doCustomBlock);
		}

		return $dlMergedTrainingWorkshops;
	}

}
