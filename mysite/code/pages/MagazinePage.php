<?php
class MagazinePage extends Page {

	private static $db = array(
		"ShowCustomBlock"	=> "Boolean",
		"ShowTestimonialInCustomBlock" => "Boolean"
	);

	private static $has_one = array(
	);

	public static $has_many = array(
	);

	public function canCreate($member = null){
		return !MagazinePage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "SampleCopyLink");

		$fields->insertBefore(new CheckboxField("ShowCustomBlock", "Show Custom Block"), 'Content');
		$fields->insertBefore(new CheckboxField("ShowTestimonialInCustomBlock", "Show Testimonial In Custom Block"), 'Content');

		return $fields;
	}

	public function CurrentIssue() {
		return MagazineIssue::get()->First();
	}

}
class MagazinePage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		"article"
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function article() {
		if(is_object($this->SelectedArticle()) &&  $this->SelectedArticle()->ID) {
			$doSelectedArticle = $this->SelectedArticle();
			$arrArticleData = array(
						"Title" => $doSelectedArticle->Title,
						"Content" => $doSelectedArticle->Content
					);

			if($doSelectedArticle->RestrictedAccess) {
				if(Member::currentUserID()) {
					$this->CreateNewArticleStatisticEntry($doSelectedArticle->ID);
					return $arrArticleData;
				} else {
					$this->redirect(Config::inst()->get('Security', 'login_url'). "?BackURL=" . urlencode($_SERVER['REQUEST_URI']));
				}
			} else {
				$this->CreateNewArticleStatisticEntry($doSelectedArticle->ID);
				return $arrArticleData;
			}
			
		} else {
			return $this->httpError("404");
		}
	}

	public function SelectedArticle(){
		$doArticle = null;
		$iParamID = $this->getRequest()->param("ID");
		$iParamID = Convert::raw2sql($iParamID);
		$doArticle =  MagazineArticle::get()->filter('URLSegment',$iParamID)->first();
		return $doArticle;
	}

	public function MergedCurrentIssueArticles() {
		$dlMergedCurrentIssueArticles = new ArrayList();

		$dlCurrentIssueArticles = $this->MagazinePage()->CurrentIssue()->Articles();

		if($this->ShowTestimonialInCustomBlock) {
			$htmlTestimonial = '<ul class="list-about-sidebar">'. $this->TestimonialWidget(2)  . '</ul>';
			$doCustomBlock = new ArrayData(
				array(
					'ClassName' => 'CustomBlock',
					'Content'	=> $htmlTestimonial
				));
		} else {
			$doCustomBlock = new ArrayData(
				array(
					'ClassName' => 'CustomBlock',
					'Content'	=> ShortcodeParser::get_active()->parse($this->Content)
				));
		}

		foreach ($dlCurrentIssueArticles as $i => $doCurrentIssueArticle) {
			if($i == 2) {
				$dlMergedCurrentIssueArticles->push($doCustomBlock);
			}

			$dlMergedCurrentIssueArticles->push($doCurrentIssueArticle);	
		}

		if($dlCurrentIssueArticles->Count() < 3) {
			$dlMergedCurrentIssueArticles->push($doCustomBlock);
		}
		
		return $dlMergedCurrentIssueArticles;
	}

	public function CreateNewArticleStatisticEntry($iArticleID) {

		$doArticleStatistic = new ArticleStatistic();
		$doArticleStatistic->MagazineArticleID = $iArticleID;
		$doArticleStatistic->write();

		return $doArticleStatistic->ID;
	}

}
