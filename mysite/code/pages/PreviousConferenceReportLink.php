<?php

class PreviousConferenceReportLink extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar(1000)",
		"Location"	=> "Varchar",
		"Type"		=> "Enum('Page,Article', 'Page')",
		"Year"		=> "Varchar"
		);

	public static $has_one = array(
		"Page"	=> "Page",
		"PreviousConferencesPage" => "PreviousConferencesPage"
		);

	public static $has_many = array(
		"Articles" => "MagazineArticle",
		);

	public static $default_sort = 'Year DESC';

	public static $summary_fields = array ("Title");

	public function getCMSFields(){

		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "PreviousConferencesPageID");
		
		$fields->addFieldToTab("Root.Main",TreeDropdownField::create("PageID", "Page Link",'SiteTree','ID','TreeTitle'));

		return $fields;
	}

	public function Data() {
		$doRet = '';

		if($this->Type == 'Article') {
			if($this->Articles()->Count()) {
				$doRet = $this->Articles()->First();
			}
		} else {
			if($this->PageID) {
				$doRet = $this->Page();
			}
		}

		return $doRet;
	}
}