<?php

class RegistrationPage extends Page {

	public static $db = array(
		'RegistratinSuccessContent' => "HTMLText",
		'SendMailAfterRegistration' => 'Boolean',
		'RegistrationEmailSubject'	=> 'Varchar',
		'RegistrationEmailTemplate'	=> "HTMLText"
	);

	public function canCreate($member = null){
		return !RegistrationPage::get()->first();
	}

	public function canDelete($member = null){
		return null;
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->addFieldsToTab("Root.Main", array(
			HTMLEditorField::Create("RegistratinSuccessContent")->SetTitle("Registratin Success Page Content")->setRightTitle("Available Placeholders '#Title' '#FirstName' '#LastName' ")
		));

		$fields->addFieldsToTab("Root.EmailSettings", array(
			new CheckboxField("SendMailAfterRegistration", "Send Mail After Registration?"),
			new TextField("RegistrationEmailSubject", "Email Subject"),
			HTMLEditorField::Create("RegistrationEmailTemplate")->SetTitle("Email Content")->setRightTitle("Available Placeholders '#Title' '#FirstName' '#LastName' ")
		));

		return $fields;
	}
}

class RegistrationPage_Controller extends Page_Controller {

	private static $allowed_actions = array (
		'RegistrationForm',
		'complete'
		);

	public function init() {
		parent::init();
	}

	public function RegistrationForm() {
		
		$fields = new FieldList(
			
			DropdownField::Create('NameTitle')->SetTitle('Title <span>*</span>')->SetSource(array('Mr'=> 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Miss' => 'Miss', 'Dr' => 'Dr', 'Sir' => 'Sir', 'Prof.' => 'Prof.')),
			
			TextField::Create('FirstName')->SetTitle('First name <span>*</span>'),
			
			TextField::Create('Surname')->SetTitle('Last Name <span>*</span>'),
			
			TextField::Create('JobTitle')->SetTitle('Job Title <span>*</span>'),
			
			TextField::Create('Organisation')->SetTitle('Organisation <span>*</span>'),
			
			TextAreaField::Create('Address')->SetTitle('Address <span>*</span>'),
			
			TextField::Create('City')->SetTitle('City <span>*</span>'),
			
			TextField::Create('State')->SetTitle('County / State'),
			
			TextField::Create('ZipCode')->SetTitle('Post / zip code'),

			CountryDropdownField::Create('Country')->SetTitle('Country'),

			PhoneNumberField::Create('Phone')->SetTitle('Phone <span>*</span>'),

			EmailField::Create('Email')->SetTitle('Email <span>*</span>'),
			
			ConfirmedPasswordField::Create('Password')->SetTitle('')
		);

		$actions = new FieldList(
			new FormAction('doRegister', 'Register')
			);

		$validator = new RequiredFields('Title', 'FirstName', 'LastName', 'JobTitle', 'Organisation', 'Address' , 'City', 'Phone', 'Email', 'Password');

		return new Form($this, 'RegistrationForm', $fields, $actions, $validator); 
	}

	public function doRegister($data,$form) {

        if($member = Member::get("Member")->filter("Email" , Convert::raw2sql($data['Email']))->first()) {
            $form->AddErrorMessage('Email', "Sorry, that email address already exists. Please choose another.", 'bad');
            Session::set("FormInfo.Form_RegistrationForm.data", $data);     
            return $this->redirectBack();;           
        }   
 
        $Member = new Member();
        $form->saveInto($Member);            
        $Member->write();
        $Member->login();
         
        if(!$userGroup = Group::get()->filter("Code" , "pmfmembers")->first()) {
            $userGroup = new Group();
            $userGroup->Code = "pmfmembers";
            $userGroup->Title = "PMFMembers";
            $userGroup->Write();
            $userGroup->Members()->add($Member);
        }
        
        $userGroup->Members()->add($Member);

        if($this->SendMailAfterRegistration) {
        	$this->SendMail($Member);
        }

        Session::set('RegisteredMemberID', $Member->ID);

        $this->redirect($this->Link('complete'));

	}

	public function SendMail($Member) {
		$strContent = $this->RegistratinSuccessContent;

		$strContent = str_replace('#Title', $Member->NameTitle ,$strContent);
		$strContent = str_replace('#FirstName', $Member->FirstName ,$strContent);
		$strContent = str_replace('#LastName', $Member->Surname ,$strContent);

		$e = new Email();
		$e->To = $currentMemeber->Email;
		$e->Subject = $this->RegistrationEmailSubject;
		$e->Body = $strContent;
		$e->send();
	}

	public function complete() {
		$strContent = $this->RegistratinSuccessContent;
		$iRegisteredMemberID = Session::get('RegisteredMemberID');
		
		if($iRegisteredMemberID) {
			$doMember = Member::get()->byID($iRegisteredMemberID);

			if(is_object($doMember)) {
				$strContent = str_replace('#Title', $doMember->NameTitle ,$strContent);
				$strContent = str_replace('#FirstName',$doMember->FirstName ,$strContent);
				$strContent = str_replace('#LastName', $doMember->Surname ,$strContent);

				Session::clear('RegisteredMemberID');

				return array("Title" => 'Registration Complete', "Content" => $strContent);
			}

		} else {
			$this->redirect($this->Link());
		}		
	}
}
