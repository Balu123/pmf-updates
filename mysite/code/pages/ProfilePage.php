<?php

class ProfilePage extends Page {
	
	public function canCreate($member = null){
		return !ProfilePage::get()->first();
	}

	public function canDelete($member = null){
		return null;
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		return $fields;
	}
}

class ProfilePage_Controller extends Page_Controller {

	private static $allowed_actions = array (
		'editprofile',
		'changepassword',
		'ProfileEditForm',
		'PasswordEditForm',
		'MembershipStatusEditForm',
		'ServicesEditForm'
		);

	public function init() {
		parent::init();

		/*$dlMember = Member::get();

		foreach ($dlMember as $member) {

			$add = $member->AddressLine1 ;

			if($member->AddressLine2) {
				$add = $add . ' , ' . $member->AddressLine2;
			}

			$member->Address = $add;
			$member->NameTitle = $member->getField('Title');
			$member->write();
		}*/


		if(!Member::currentUserID()) {
			$this->redirect(Config::inst()->get('Security', 'login_url'). "?BackURL=" . urlencode($_SERVER['REQUEST_URI']));
		}
	}

	public function editprofile(){
		return $arrArticleData = array("PageHeading" => "Edit Profile", "Title" =>'Edit Profile',"Content" => '<p>Change your details below. Elements marked with * are mandatory</p>');
	}

	public function ProfileEditForm() {
		
		$doMember = Member::currentUser();

		$fields = new FieldList(
			
			DropdownField::Create('NameTitle')->SetTitle('Title <span>*</span>')->setValue($doMember->NameTitle)->SetSource(array('Mr'=> 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Miss' => 'Miss', 'Dr' => 'Dr', 'Sir' => 'Sir', 'Prof.' => 'Prof.')),
			
			TextField::Create('FirstName')->SetTitle('First name <span>*</span>')->setValue($doMember->FirstName),
			
			TextField::Create('Surname')->SetTitle('Last Name <span>*</span>')->setValue($doMember->Surname),
			
			TextField::Create('JobTitle')->SetTitle('Job Title <span>*</span>')->setValue($doMember->JobTitle),
			
			TextField::Create('Organisation')->SetTitle('Organisation <span>*</span>')->setValue($doMember->Organisation),
			
			TextAreaField::Create('Address')->SetTitle('Address <span>*</span>')->setValue($doMember->Address),
			
			TextField::Create('City')->SetTitle('City <span>*</span>')->setValue($doMember->City),
			
			TextField::Create('State')->SetTitle('County / State')->setValue($doMember->State),
			
			TextField::Create('ZipCode')->SetTitle('Post / zip code')->setValue($doMember->ZipCode),

			CountryDropdownField::Create('Country')->SetTitle('Country')->setValue($doMember->Country),

			TextField::Create('Phone')->SetTitle('Phone <span>*</span>')->setValue($doMember->Phone),

			EmailField::Create('Email')->SetTitle('Email <span>*</span>')->setValue($doMember->Email)
		);

		$actions = new FieldList(
			new FormAction('doProfileEdit', 'Save')
			);

		$validator = new RequiredFields('Title', 'FirstName', 'LastName', 'JobTitle', 'Organisation', 'Address' , 'City', 'Phone', 'Email');

		return new Form($this, 'ProfileEditForm', $fields, $actions, $validator); 
	}

	public function doProfileEdit($data,$form) {

		$doMember = Member::currentUser();

        if($member = Member::get("Member")->filter("Email" , Convert::raw2sql($data['Email']))->exclude("Email" , $doMember->Email)->first()) {
            $form->AddErrorMessage('Email', "Sorry, that email address already exists. Please choose another.", 'bad');
            Session::set("FormInfo.Form_ProfileEditForm.data", $data);     
            return $this->redirectBack();;           
        }   
 
        $form->saveInto($doMember);            
       	$doMember->write();
    
        $this->redirect($this->Link());

	}

	public function changepassword(){
		return $arrArticleData = array("PageHeading" => "Change Password", "Title" =>'Change Password',"Content" => '<p>Change your password below. Elements marked with * are mandatory</p>');
	}

	public function PasswordEditForm() {

		$fields = new FieldList(
			ConfirmedPasswordField::Create('Password')
		);

		$actions = new FieldList(
			new FormAction('doPasswordEdit', 'Register')
			);

		$validator = new RequiredFields('Title', 'FirstName', 'LastName', 'JobTitle', 'Organisation', 'Address' , 'City', 'Phone', 'Email', 'Password');

		return new Form($this, 'PasswordEditForm', $fields, $actions, $validator); 
	}

	public function doPasswordEdit($data,$form) {
		$doMember = Member::currentUser();

		$form->saveInto($doMember);            
       	$doMember->write();
    
        $this->redirect($this->Link());
	}


	public function MembershipStatusEditForm() {

		$doMember = Member::currentUser();

		$fields = new FieldList(

			CheckboxField::Create('PMFArticleBank')->SetTitle('Article Bank access')->setValue($doMember->PMFArticleBank)
			
		);

		$actions = new FieldList(
			new FormAction('doMembershipStatusEdit', 'Save')
			);

		$validator = new RequiredFields();

		return new Form($this, 'MembershipStatusEditForm', $fields, $actions, $validator); 
	}

	public function doMembershipStatusEdit($data,$form) {
		$doMember = Member::currentUser();

		$form->saveInto($doMember);            
		$doMember->write();

		$this->redirect($this->Link());
	}

	public function ServicesEditForm() {

		$doMember = Member::currentUser();

		$fields = new FieldList(
			
			CheckboxField::Create('PMFDirectory')->SetTitle('Inclusion in online Members\' Directory')->setValue($doMember->PMFDirectory),

			new LiteralField('PMFDirectoryInfo','<label for="usr">Only those included in the directory have access to view other members\' contact details. (members only)</label>'),
			
			/*CheckboxField::Create('FirstName')->SetTitle('First name <span>*</span>')->setValue($doMember->FirstName),
			
			CheckboxField::Create('Surname')->SetTitle('Last Name <span>*</span>')->setValue($doMember->Surname),*/

			CheckboxField::Create('PMFArticleBank')->SetTitle('Article Bank access')->setValue($doMember->PMFArticleBank)
			
		);

		$actions = new FieldList(
			new FormAction('doServicesEdit', 'Save')
			);

		$validator = new RequiredFields();

		return new Form($this, 'ServicesEditForm', $fields, $actions, $validator); 
	}

	public function doServicesEdit($data,$form) {
		$doMember = Member::currentUser();

		$form->saveInto($doMember);            
		$doMember->write();

		$this->redirect($this->Link());
	}

	public function GetActionType() {
		return $this->getRequest()->param("Action");
	}

}
