<?php
class CareerGuidancePage extends Page {

	private static $db = array(
		);

	private static $has_one = array(
		);

	public static $has_many = array(
		"Books"	=> "Book"
		);

	public function canCreate($member = null){
		return !CareerGuidancePage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		return $fields;
	}

}
class CareerGuidancePage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function PaginatedCarrerGuidanceArticles() {
		$dlCareerGuidanceArticles = new ArrayList();

		$doCareerChannel = ArticleChannel::get()->filter('Title', 'Career Guidance')->first();

		if(is_object($doCareerChannel) && $doCareerChannel->ID) {
			$dlCareerGuidanceArticles = MagazineArticle::get()->filter('ArticleChannels.ID', $doCareerChannel->ID);
		}

		$dlPaginatedCarrerGuidanceArticles = new PaginatedList($dlCareerGuidanceArticles, $this->request);
		$dlPaginatedCarrerGuidanceArticles->setPageLength($this->ShowNumberOfItems());
		
		return $dlPaginatedCarrerGuidanceArticles;
	}

	public function SearchViewNoOfItemsLink(){
		return  $this->request->getURL(true) . '&show=';
	}

	public function ShowNumberOfItems(){
		return  isset($_REQUEST['show']) ? $_REQUEST['show'] : 24;
	}
}
