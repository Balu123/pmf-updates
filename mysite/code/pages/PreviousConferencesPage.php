<?php
class PreviousConferencesPage extends Page {

	private static $db = array(
	);

	private static $has_one = array(
	);

	public static $has_many = array(
		"PreviousConferenceReportLinks" => "PreviousConferenceReportLink"
	);

	private static $allowed_children = array('SnapshotPage');

	public function canCreate($member = null){
		return !PreviousConferencesPage::get()->first();
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName("Widgets");

		$cfgPreviousConferenceReportLinks = new GridFieldConfig_RelationEditor();
		$cfgPreviousConferenceReportLinks->removeComponent($cfgPreviousConferenceReportLinks->getComponentByType('GridFieldDeleteAction'));
		$cfgPreviousConferenceReportLinks->removeComponent($cfgPreviousConferenceReportLinks->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgPreviousConferenceReportLinks->addComponent(new GridFieldDeleteAction());
		$fields->addFieldToTab("Root.PreviousConferenceReportLinks", new GridField("PreviousConferenceReportLinks", "PreviousConferenceReportLinks", $this->PreviousConferenceReportLinks(),$cfgPreviousConferenceReportLinks));
	  	

		return $fields;
	}

}

class PreviousConferencesPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function LatestConferenceReport() {
		return PreviousConferenceReportLink::get()->Filter(array('PreviousConferencesPageID' => $this->ID))->first();
	}
}
