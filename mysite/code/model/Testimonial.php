<?php

class Testimonial extends DataObject{
	
	public static $db = array(
		"Author" => "Varchar",
		"Designation" => "Varchar",
		"Company" => "Varchar",
		"ShowOnHomePage" => "Boolean",
		"Content" => "Text",
		"SortOrder" => "Int"
	);

	public static $has_one = array(
		"TestimonialPage"	=> "TestimonialPage",
	);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array (
		'Content',
		'Author',
		'ShowOnHomePage'
		);

	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "TestimonialPageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
		return $fields;
	}
}