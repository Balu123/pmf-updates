<?php

class ArticleKeyword extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(200)",
	);

	public static $has_one = array(
		"ArticleKeywordGroup"	=> "ArticleKeywordGroup"
	);

	public static $belongs_many_many = array(
		"MagazineArticles"	=> "MagazineArticle"
	);

	public static $summary_fields = array ('Title' => 'Title', 'ArticleKeywordGroup.Title' => 'Keyword Group');

	public function getCMSFields(){
		$fields=parent::getCMSFields();			
		return $fields;
	}
}