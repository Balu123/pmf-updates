<?php

class ArticleChannel extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(200)",
	);

	public static $belongs_many_many = array(
		"MagazineArticles"	=> "MagazineArticle"
	);

	public static $summary_fields = array ('Title');

	public function getCMSFields(){
		$fields=parent::getCMSFields();			
		return $fields;
	}
}