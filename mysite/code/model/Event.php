<?php

class Event extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar(1000)",
		"Date"		=> "SS_Datetime",
		"MoreInfoLink" => "Varchar(1000)",
		"Summary"		=> "Text",
		"Description" => "HTMLText"
		);

	public static $has_one = array(
		"LocationPage"	=> "LocationPage",
		);

	public static $default_sort = 'Date ASC';

	public static $summary_fields = array ("Title", "FormatedDate");

	static $create_table_options = array(
		'MySQLDatabase' => 'ENGINE=MyISAM'
		);
	
	public function getCMSFields(){

		Requirements::javascript('themes/pmf/javascript/jquery.timepicker.min.js');
		Requirements::javascript('themes/pmf/javascript/timefield.js');
		Requirements::css('themes/pmf/css/jquery.timepicker.css');

		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "LocationPageID");
		$fields->removeFieldFromTab("Root.Main", "Date");

		$DateField = DateTimeField::create('Date')->setTitle('Date');

		$DateField->getDateField()->setConfig('showcalendar', 1);
		$fields->insertBefore($DateField, 'MoreInfoLink');

		return $fields;
	}

	public function FormatedDate() {
		return  $this->obj('Date')->format('l j F Y');
	}
}