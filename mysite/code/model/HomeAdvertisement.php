<?php

class HomeAdvertisement extends DataObject{
	
	public static $db = array(
		"Title"	=> "Varchar(1000)",
		"SortOrder" => "Int"
	);

	public static $has_one = array(
		"HomePage"	=> "HomePage",
		"Image" => "Image",
		"InternalLink"	=> "Page"
	);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array (
		'Title',
		'Thumb'
		);

	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "HomePageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");

		$fields->insertBefore(TreeDropdownField::create("InternalLinkID", "Link",'SiteTree','ID','TreeTitle'), 'Image');

		return $fields;
	}

	public function Thumb(){
		return $this->Image()->SetWidth('150');
	}
}