<?php 

class ContactUsFormSubmission extends DataObject {

	private static $db = array(
		'Name' => 'Text',
		'Email' => 'Text',
		'Message' => 'Text'
		);

	public static $has_one = array(
		"ContactPage"	=> "ContactPage",
	);

	public static $default_sort = 'Date ASC';

	public static $summary_fields = array ("Title", "SubmissionDate");

	public function getCMSFields(){

		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "ContactPageID");

		return $fields;
	}

	public function SubmissionDate() {
		return  $this->obj('DateCreated')->format('l j F Y');
	}
}