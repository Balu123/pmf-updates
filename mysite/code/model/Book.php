<?php

class Book extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(100)",
		"Authors" => "Varchar",
		"Publication" => "Varchar",
		"Price"		=> "Varchar",
		"PurchaseLink" => "Varchar(1000)",
		"BookOfTheMonth" => "Boolean"
	);

	public static $has_one = array(
		"BookReviewsPage"	=> "BookReviewsPage",
		"Review"		=> "MagazineArticle",
		"Image"			=> "Image",
	);

	public static $default_sort = 'Created ASC';

	public static $summary_fields = array ('Title' => 'Title', 'Authors' => 'Authors', 'ReviewLink' => 'Review Article Link');

	static $create_table_options = array(
		'MySQLDatabase' => 'ENGINE=MyISAM'
		);
	
	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "BookReviewsPageID");
		$fields->removeFieldFromTab("Root.Main", "Image");

		$uploadField = new UploadField("Image", "Image");
		$uploadField->setFolderName('Uploads/books');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");
		$fields->addFieldToTab("Root.Main",$uploadField );

		return $fields;
	}

	public function ReviewLink() {
		$strRet = '';

		if($this->ReviewID) {
			$strRet = $this->Review()->Link();
		}

		return $strRet;
	}
}