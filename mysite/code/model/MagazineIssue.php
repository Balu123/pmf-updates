<?php

class MagazineIssue extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar(1000)",
		"Year"		=> "Varchar",
		"Volume" => "Int",
		"Issue" => "Int",
		"Publish" => "Boolean"
	);

	public static $has_one = array(
		"Source"		=> "ArticleSource",
		"CoverImage"	=> "Image"
	);

	public static $has_many = array(
		"Articles"	=> "MagazineArticle"
	);

	public static $default_sort = 'Year DESC, Volume DESC, Issue DESC';

	public static $summary_fields = array("Title" => "Title", "Year" => "Year", "Volume" => "Volume", "Issue" => "Issue", "Source.Title" => "Source", "Publish" => "Publish");

	public function getCMSFields(){

		Requirements::javascript('themes/pmf/javascript/jquery.timepicker.min.js');
        Requirements::javascript('themes/pmf/javascript/timefield.js');
        Requirements::css('themes/pmf/css/jquery.timepicker.css');

		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "MagazinePageID");
		$fields->removeByName('Articles');

		$cfgArticles = new GridFieldConfig_RelationEditor();
		$cfgArticles->removeComponent($cfgArticles->getComponentByType('GridFieldDeleteAction'));
		$cfgArticles->removeComponent($cfgArticles->getComponentByType('GridFieldAddExistingAutocompleter'));
		$cfgArticles->addComponent(new GridFieldDeleteAction());
		$cfgArticles->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.Articles", new GridField("Articles", "Articles", $this->Articles(),$cfgArticles ));


		return $fields;
	}
	
	public function FormatedDate() {
		return  $this->FormatedMonth . ' ' . $this->FormatedYear();
	}

	public function FormatedYear() {
		return date("Y", strtotime(trim($this->Year))); 
	}

	public function FormatedMonth() {
		return date("M", strtotime(trim($this->Title)));
	}

	public function IssueDetailsText() {
		return 'Volume : ' . $this->Volume . ' Issue : ' . $this->Issue;
	}
}