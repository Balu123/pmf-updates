<?php

class HomeBanner extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar(100)",
		"Content"		=> "Varchar(100)",
		"ExternalLink"	=> "Varchar(500)",
		"LinkType"	=> "Enum('External Link,Internal Link','External Link')",
		"OpenInNewWindow" => "Boolean",
		"SortOrder"	=> "Int",
	);

	public static $has_one = array(
		"HomePage"	=> "HomePage",
		"InternalLink" => "Page",
		"BannerImage" => "Image"
	);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array (
		'Title',
		'Thumb'
		);

	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "HomePageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
		$fields->removeFieldFromTab("Root.Main", "InternalLinkID");
		$fields->removeFieldFromTab("Root.Main", "ExternalLink");

		$fields->addFieldToTab("Root.Main", new headerField("Link"));
		$fields->addFieldToTab("Root.Main",new SelectionGroup(
				"LinkType",
				array(
					'External Link' => TextField::create("ExternalLink", "Link"),
					'Internal Link' => TreeDropdownField::create("InternalLinkID", "Link",'SiteTree','ID','TreeTitle'),
				)
			)
		);
		$fields->addFieldToTab("Root.Main", new CheckboxField("OpenInNewWindow", "Open In New Window"));

		return $fields;
	}

	public function Thumb(){
		return $this->BannerImage()->SetWidth('150');
	}

	public function Link(){
		$strLink = '';
		if($this->ExternalLink){
			$strLink = $this->ExternalLink;
		} elseif($this->InternalLinkID){
			$strLink = $this->InternalLink()->Link();
		}
		return $strLink;
	}
}