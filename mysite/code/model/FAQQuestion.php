<?php

class FAQQuestion extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(200)",
		"Content" => "HTMLText",
		"SortOrder"	=> "Int"
	);

	public static $has_one = array(
		"FAQPage"	=> "FAQPage"
	);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array ('Title');

	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "FAQPageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
			
		return $fields;
	}
}