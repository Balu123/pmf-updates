<?php

class ArticleSection extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(200)",
	);

	public static $has_one = array(
		"ArticleSource"	=> "ArticleSource"
	);

	public static $has_many = array(
		"MagazineArticles"	=> "MagazineArticle"
	);

	public static $summary_fields = array ('Title');

	public function getCMSFields(){
		$fields=parent::getCMSFields();			
		return $fields;
	}

	public function TitleWithSource() {

		$strSection = $this->ArticleSource()->Title ?  $this->ArticleSource()->Title : 'pm';
		return $this->Title . ' (' . $strSection   . ')';
	}
}