<?php

class ArticleStatistic extends DataObject{
	
	public static $db = array(
	);

	public static $has_one = array(
		"MagazineArticle"	=> "MagazineArticle",
	);

	/*public static $summary_fields = array(
		"MagazineArticle.StatisticsPastDay" => "Past Day",
		"MagazineArticle.StatisticsPastWeek" => "Past Week",
		"MagazineArticle.StatisticsPastMonth" => "Past Month",
		"MagazineArticle.StatisticsPastYear" => "Past Year",
		"MagazineArticle.StatisticsPreviousYear" => "Previous Year",
	);*/

	public function getCMSFields(){
		$fields=parent::getCMSFields();			
		return $fields;
	}

	public function getTitle() {
		return $this->MagazineArticle()->Title;
	}
}