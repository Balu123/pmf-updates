<?php

class PMFStaffMembers extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(100)",
		"SortOrder"	=> "Int"
	);

	public static $has_one = array(
		"ContactPage"	=> "ContactPage",
		"Image"			=> "Image",
	);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array ('Title', 'Thumb');

	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "ContactPageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
		$fields->removeFieldFromTab("Root.Main", "Image");
			
		$uploadField = new UploadField("Image", "Image");
		$uploadField->setFolderName('Uploads/staff-members');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");
		$fields->addFieldToTab("Root.Main",$uploadField );

		return $fields;
	}

	public function Thumb(){
		return $this->Image()->SetWidth('50');
	}
}