<?php

class TrainingWorkshop extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar(1000)",
		"DateStart"		=> "SS_Datetime",
		"DateEnds"		=> "SS_Datetime",
		"Level"			=> "Varchar",
		"Location"		=> "Varchar",
		"Duration"		=> "Enum('Half Day,Full Day')",
		"Description" => "HTMLText"
	);

	public static $has_one = array(
		"BookingForm"	=> "File",
		"TrainingPage"	=> "TrainingPage"
	);
	
	public function populateDefaults() {
 		parent::populateDefaults();
	}

	public static $default_sort = 'DateStart ASC';

	public static $summary_fields = array ("Title", "Date");

	static $create_table_options = array(
		'MySQLDatabase' => 'ENGINE=MyISAM'
		);
	
	public function getCMSFields(){

		Requirements::javascript('themes/pmf/javascript/jquery.timepicker.min.js');
        Requirements::javascript('themes/pmf/javascript/timefield.js');
        Requirements::css('themes/pmf/css/jquery.timepicker.css');

		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "TrainingPageID");
		$fields->removeFieldFromTab("Root.Main", "DateStart");
		$fields->removeFieldFromTab("Root.Main", "BookingForm");

		$DateStartField = DateTimeField::create('DateStart')->setTitle('Date/Time Starts');
		$DateStartField->getDateField()->setConfig('showcalendar', 1);
		$fields->insertBefore($DateStartField, 'Location');

		$DateEndsField = DateTimeField::create('DateEnds')->setTitle('Date/Time Ends');
		$DateEndsField->getDateField()->setConfig('showcalendar', 1);
		$fields->insertBefore($DateEndsField, 'Location');

		$LevelField =  new CheckboxSetField( $name = "Level", $title = "Level", $source = array( "Beginners" => "Beginners", "Intermediate" => "Intermediate", "Advanced" => "Advanced", "All" => "All"));
		$fields->insertBefore($LevelField, 'Location');

		$uploadField = new UploadField("BookingForm", "Booking Form");
		$uploadField->setFolderName('Uploads/training-workshop-forms');
		$uploadField->getValidator()->allowedExtensions = array('doc','docx','pdf');
		$fields->insertBefore($uploadField, 'Description');
	
		return $fields;
	}

	public function Date() {
		$dtStartDate =  $this->obj('DateStart')->format('l j F Y');
		return $dtStartDate ;
	}

	public function Time() {
		$dtStartTime = $this->obj('DateStart')->format('g.ia');
		$dtTimeEnds =  	$this->obj('DateEnds')->format('g.ia');

		return $dtStartTime . ' to ' . $dtTimeEnds ;
	}

	public function Link() {
		$strLink = '#kt' . $this->obj('DateStart')->format('jFY');
		return $strLink;
	}

	public function LevelImage() {
		$arrLevels = explode(',', $this->Level);

		$strImages = '';

		foreach ($arrLevels as $level) {
			$strImages .= '<img src="themes/pmf/images/' . $level . '.gif" width="19" height="19" alt="Beginners" >';
		}

		return $strImages;
	}
}