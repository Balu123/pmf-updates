<?php

class CommitteeMember extends DataObject{
	
	public static $db = array(
		"Title" => "Varchar(100)",
		"Designation" => "Varchar(100)",
		"Email" => "Varchar(100)",
		"Description" => "Varchar(100)",
		"SortOrder"	=> "Int"
	);

	public static $has_one = array(
		"LocationPage"	=> "LocationPage",
		"Image"			=> "Image",
	);

	public static $default_sort = 'SortOrder ASC';

	public static $summary_fields = array ('Title', 'Thumb');

	public function getCMSFields(){
		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "LocationPageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
		$fields->removeFieldFromTab("Root.Main", "Image");
			
		$uploadField = new UploadField("Image", "Image");
		$uploadField->setFolderName('Uploads/committee-members');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");
		$fields->addFieldToTab("Root.Main",$uploadField );

		return $fields;
	}

	public function Thumb(){
		return $this->Image()->SetWidth('50');
	}
}