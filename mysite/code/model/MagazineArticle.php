<?php

class MagazineArticle extends DataObject{
	
	public static $db = array(
		"Title"		=> "Varchar(1000)",
		"URLSegment"	=> "Varchar(500)",
		"RestrictedAccess" => "Boolean",
		"PublicationDate" => "SS_Datetime",
		"BookAuthor"	=> "Varchar",
		"EventSpeaker" => "Varchar",
		"Summary"	=> "HTMLText",
		"ArticleSubject" => "Text",
		"Content"	=> "HTMLText",
		"Authors" => "Text",
		"AuthorsString" => "Text",
		"AuthorsSubject" => "Text"
	);

	public static $has_one = array(
		"Image"			=> "Image",
		"MagazineIssue"	=> "MagazineIssue",
		"ArticleSource"	=> "ArticleSource",
		"ArticleSection" => "ArticleSection",
		"PreviousConferenceReportLink" => "PreviousConferenceReportLink"
	);

	public static $has_many = array(
		"ArticleStatistics"	=> "ArticleStatistic"
	);

	public static $many_many = array(
		"ArticleKeywords"	=> "ArticleKeyword",
		"ArticleChannels" => "ArticleChannel",
	);

	public static $default_sort = 'PublicationDate DESC';

	public static $summary_fields = array(
		"Title" => "Title",
		"Authors" => "Author",
		"MagazineIssue.Volume" => "Volume",
		"MagazineIssue.Issue" => "Issue",
		"MagazineIssue.Year" => "Year",
		"MagazineIssue.Title" => "Issue Title",
		"ArticleSource.Title" => "Source",
		"ArticleSection.TitleWithSource" => "Section",
		"IsRestrictedString" => "Visibilty"
	);

	static $create_table_options = array(
		'MySQLDatabase' => 'ENGINE=MyISAM'
		);
	
	public function getCMSFields(){

		$fields=parent::getCMSFields();
		$fields->removeFieldFromTab("Root.Main", "MagazineIssueID");
		$fields->removeFieldFromTab("Root.Main", "ArticleSourceID");
		$fields->removeFieldFromTab("Root.Main", "ArticleSectionID");
		$fields->removeFieldFromTab("Root.Main", "PreviousConferenceReportLinkID");
		$fields->removeFieldFromTab("Root.Main", "Authors");
		$fields->removeFieldFromTab("Root.Main", "AuthorsString");
		$fields->removeFieldFromTab("Root.Main", "AuthorsSubject");
		$fields->removeFieldFromTab("Root.Main", "Content");
		$fields->removeFieldFromTab("Root.Main", "Summary");
		$fields->removeByName("ArticleKeywords");
		$fields->removeByName("ArticleChannels");
		$fields->removeByName("ArticleStatistics");

		// Main Tab

		$field = DropdownField::create('MagazineIssueID', 'Volume/Issue', MagazineIssue::get()->map('ID', 'IssueDetailsText'))->setEmptyString('(Select one)');
		$fields->addFieldToTab('Root.Main', $field);

		$field = DropdownField::create('ArticleSourceID', 'Source', ArticleSource::get()->map('ID', 'Title'))->setEmptyString('(Select one)');
		$fields->addFieldToTab('Root.Main', $field);

		$field = DropdownField::create('ArticleSectionID', 'Section', ArticleSection::get()->map('ID', 'TitleWithSource'))->setEmptyString('(Select one)');
		$fields->addFieldToTab('Root.Main', $field);

		$strLink = '<b> Frontend Link </b><a href="' .  $this->Link() . '">'. $this->Link() . '</a>';
		$linkField = new LiteralField ("literalfield",$strLink);
   		$fields->addFieldToTab('Root.Main', $linkField);

		// Content Tab
   		$ArticleSubjectField = new TextareaField('ArticleSubject', 'Article Subject');
		$fields->addFieldToTab('Root.Content', $ArticleSubjectField);

		$field = HTMLEditorField::create('Summary', 'Summary')->setRows(5);
		$fields->addFieldToTab('Root.Content', $field);

		$uploadField = new UploadField("Image", "Image");
		$uploadField->setFolderName('Uploads/article-images');
		$uploadField->getValidator()->allowedExtensions = array("gif","jpg" ,"jpeg","png");
		$fields->addFieldToTab("Root.Main",$uploadField );

		$field = HTMLEditorField::create('Content', 'Content');
		$fields->addFieldToTab('Root.Content', $field);


		//Authors Tab
		$AuthorsDetailField = new TextareaField('Authors', 'Authors Detail');
		$fields->addFieldToTab('Root.Authors', $AuthorsDetailField);

		$AuthorsDetailField = new TextareaField('AuthorsSubject', 'Authors Subject');
		$fields->addFieldToTab('Root.Authors', $AuthorsDetailField);

		$AuthorsDetailField = new HTMLEditorField('AuthorsString', 'Authors String');
		$AuthorsDetailField->setRows(5);
		$fields->addFieldToTab('Root.Authors', $AuthorsDetailField);

		//Keywords Tab

		$cfgKeywords = new GridFieldConfig_RelationEditor();
		$fields->addFieldToTab("Root.Keywords", new GridField("Keywords", "Keywords", $this->ArticleKeywords(),$cfgKeywords ));

		// Channles

		$cfgChannels = new GridFieldConfig_RelationEditor();
		$fields->addFieldToTab("Root.Channels", new GridField("Channels", "Channels", $this->ArticleChannels(),$cfgChannels ));

		// Statistics
		
		//$fields->addFieldToTab("Root.Statistics", new GridField("Statistics", "Statistics", $this->ArticleStatistics(),$cfgStatistics ));

		return $fields;
	}

	public function onBeforeWrite(){
		parent::onBeforeWrite();
		$title = trim($this->Title);
		if((!$this->URLSegment || $this->URLSegment == 'article-page') && $this->Title) {
			$filter = URLSegmentFilter::create();
			$t = $filter->filter($title);
			if(!$t || $t == '-' || $t == '-1') $t = "article-$this->ID";
			$this->URLSegment = $t;
		} else if($this->isChanged('URLSegment', 2)) {
			$filter = URLSegmentFilter::create();
			$this->URLSegment = $filter->filter($this->URLSegment);
			if(!$this->URLSegment) $this->URLSegment = "article-$this->ID";
		}

		$count = 2;
		while($this->LookForExistingURLSegment($this->URLSegment)) {
			$this->URLSegment = preg_replace('/-[0-9]+$/', null, $this->URLSegment) . '-' . $count;
			$count++;
		}
	}

	public function LookForExistingURLSegment($strURLSegment){
		return MagazineArticle::get()->filter("URLSegment", $strURLSegment)->exclude('ID',$this->ID)->first();
	}

	public function Link(){
		$doMagazinePage = MagazinePage::get()->First();
		$strSubLink = 'article/' . $this->URLSegment;
		$strLink = $doMagazinePage->Link($strSubLink);
		return $strLink;
	}

	public function IsRestrictedString() {
		if($this->RestrictedAccess) {
			$strRet = "Restricted";
		} else {
			$strRet = "Open to all";
		}

		return $strRet;
	}

	public function StatisticsPastDay() {
		$curDate = date("Y-m-d");
		$dtPastDayDateMin = date('Y-m-d', strtotime($curDate .' -1 day'));
		$dtPastDayDateMax = date('Y-m-d', strtotime($curDate .' -2 day'));
		$iPastDayCount = $this->ArticleStatistics()->filter(array('Created:GreaterThan' => $dtPastDayDateMax, 'Created:LessThan' => $dtPastDayDateMin))->Count();
		return $iPastDayCount;
	}

	public function StatisticsPastWeek() {
		$curDate = date("Y-m-d");
		$dtPastWeekDateMin = date('Y-m-d', strtotime($curDate .' -1 week'));
		$dtPastWeekDateMax = date('Y-m-d', strtotime($curDate .' -2 week'));
		$iPastWeekCount = $this->ArticleStatistics()->filter(array('Created:GreaterThan' => $dtPastWeekDateMax, 'Created:LessThan' => $dtPastWeekDateMin))->Count();
		return $iPastWeekCount;
	}

	public function StatisticsPastMonth() {
		$curDate = date("Y-m-d");
		$dtPastMonthDateMin = date('Y-m-d', strtotime($curDate .' -1 month'));
		$dtPastMonthDateMax = date('Y-m-d', strtotime($curDate .' -2 month'));
		$iPastMonthCount = $this->ArticleStatistics()->filter(array('Created:GreaterThan' => $dtPastMonthDateMax, 'Created:LessThan' => $dtPastMonthDateMin))->Count();
		return $iPastMonthCount;
	}

	public function StatisticsPastYear() {
		$curDate = date("Y-m-d");
		$dtPastYearDateMin = date('Y-m-d', strtotime($curDate .' -1 year'));
		$dtPastYearDateMax = date('Y-m-d', strtotime($curDate .' -2 year'));
		$iPastYearCount = $this->ArticleStatistics()->filter(array('Created:GreaterThan' => $dtPastMonthDateMax, 'Created:LessThan' => $dtPastMonthDateMin))->Count();
		return $iPastYearCount;
	}

	public function StatisticsPreviousYear() {
		$curDate = date("Y-m-d");
		$dtPreviousYearMin = date('Y-m-d', strtotime($curDate .' -2 year'));
		$iPreviousYearCount = $this->ArticleStatistics()->filter(array('Created:LessThan' => $dtPreviousYearMin))->Count();
		return $iPreviousYearCount;
	}
}