<?php

class CustomSiteConfig extends DataExtension{

	public static $db = array(
		'TwitterLink' 	=> 'Varchar(1000)',
		'LinkedInLink' 	=> 'Varchar(1000)'
	);


	public function UpdateCMSFields(FieldList $fields){
		$fields->removeByName("Theme");

		$fields->addFieldsToTab("Root.SocialMedia", array(
			new TextField("TwitterLink", "Twitter Link"),
			new TextField("LinkedInLink", "LinkedIn Link")
		));
	}
}