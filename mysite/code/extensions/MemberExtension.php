<?php

class MemberExtension extends DataExtension{

	public static $db = array(
		"Title" => "Varchar",
		"NameTitle" => "Varchar",
		"JobTitle" => "Varchar",
		"Organisation" => "Varchar",
		"Address"	=> "Text",
		"AddressLine1"	=> "Text",
		"AddressLine2"	=> "Text",
		"City"		=> "Varchar",
		"State"		=> "Varchar",
		"ZipCode"	=> "Varchar",
		"Country"	=> "Varchar",
		"Phone"		=> "Varchar",
		"Sector"	=> "Varchar",
		"PMFArticleBank" => "Boolean",
		"PMFDirectory"	=> "Boolean",
		"PMFStatus"	=> "Varchar",
		"PMMag"		=> "Varchar",
		"PMFMailingLists" => "Varchar(100)",
		"PMFUpdate"	=> "Varchar",
	);

	public static $create_table_options = array(
		'MySQLDatabase' => 'ENGINE=MyISAM'
		);

	public static $summary_fields = array ("Organisation" => "Organisation", "JobTitle" => "Job Title");
}